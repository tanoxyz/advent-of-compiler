
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>


#define AOC_ARENA_CAPACITY (4*1024*1024) // 4mb
typedef struct AOC_Arena AOC_Arena;
struct AOC_Arena {
    AOC_Arena *prev;
    AOC_Arena *current;
    int64_t cursor;
    uint8_t data[AOC_ARENA_CAPACITY];
};

AOC_Arena *AOC_free_arenas = NULL;

AOC_Arena *
AOC_Arena_Allocate(void) {
    AOC_Arena *result = NULL;
    if (AOC_free_arenas) {
        result = AOC_free_arenas;
        AOC_free_arenas = AOC_free_arenas->prev;
    }
    else {
        uint8_t *allocated_data = sbrk(sizeof(AOC_Arena)+(sizeof(void*)-1));
        uintptr_t allocated_data_int = (uintptr_t)allocated_data;
        allocated_data_int = (allocated_data_int+(sizeof(void*)-1))&(~(sizeof(void*)-1)); // Align
        result = (AOC_Arena *)allocated_data_int;
    }
    result->prev = NULL;
    result->current = result;
    result->cursor = 0;
    return result;
}

void
AOC_Arena_Release(AOC_Arena *arena) {
    arena->prev = AOC_free_arenas;
    AOC_free_arenas = arena->current;
}

void *
AOC_Arena_Push_aligned(AOC_Arena *arena, int64_t total_bytes, int64_t alignement) {
    AOC_Arena *arena_curr   = arena->current;
    uintptr_t start_address = (uintptr_t)(arena_curr->data+arena_curr->cursor);
    uintptr_t end_address   = (uintptr_t)(arena_curr->data+AOC_ARENA_CAPACITY);
    uintptr_t start_address_aligned = (start_address+(alignement-1))-((start_address+(alignement-1))%alignement);
    arena_curr->cursor     += (start_address_aligned+total_bytes)-start_address;
    void *result = (void *)start_address_aligned;

    // Not enough memory in the current arena, allocate a new arena
    if (start_address_aligned+total_bytes > end_address) {
        AOC_Arena *arena_new = AOC_Arena_Allocate();
        start_address = (uintptr_t)(arena_new->data);
        end_address   = (uintptr_t)(arena_new->data+AOC_ARENA_CAPACITY);
        start_address_aligned = (start_address+(alignement-1))-((start_address+(alignement-1))%alignement);

        if (start_address_aligned+total_bytes > end_address) {
            result = NULL; // Didn't fit anyway
        }
        else {
            arena_new->cursor    = (start_address_aligned+total_bytes)-start_address;
            result = (void *)start_address_aligned;
        }

        arena_new->prev = arena_curr;
        arena->current  = arena_new;
    }

    return result;
}


#define AOC_ARENA_NEW(arena, T) ((T*)AOC_Arena_Push_aligned(arena, sizeof(T), sizeof(void*)))
#define AOC_ARENA_NEW_ZERO(arena, T) ((T*)memset(AOC_Arena_Push_aligned(arena, sizeof(T), sizeof(void*)),0,sizeof(T)))



#define AOC_ARRLEN(a) (sizeof(a)/sizeof(a[0]))

bool
AOC_Rune_match(uint32_t rune, uint32_t match_array[], int64_t match_array_count) {
    for (int64_t i=0; i < match_array_count; i+=1) {
        if (rune == match_array[i]) return true;
    }
    return false;
}

bool
AOC_Rune_is_whitespace(uint32_t rune) {
    static uint32_t match_array[] = {' ', '\n', '\r', '\t'};
    bool result = AOC_Rune_match(rune, match_array, AOC_ARRLEN(match_array));
    return result;
}

bool
AOC_Rune_is_alpha(uint32_t rune) {
    static uint32_t match_array[] = {
        'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v',
        'w','x','y','z', 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R',
        'S','T','U','V','W','X','Y','Z'
    };
    bool result = AOC_Rune_match(rune, match_array, AOC_ARRLEN(match_array));
    return result;
}

bool
AOC_Rune_is_numeric(uint32_t rune) {
    static uint32_t match_array[] = {'1','2','3','4','5','6','7','8','9','0'};
    bool result = AOC_Rune_match(rune, match_array, AOC_ARRLEN(match_array));
    return result;
}

bool
AOC_Rune_is_symbol(uint32_t rune) {
    static uint32_t match_array[] = {
        '/','.',':',';',',','[',']','<','>','!','%','^','&','*','(',')','{','}','-','=','+','|',
        '~','"','\''
    };
    bool result = AOC_Rune_match(rune, match_array, AOC_ARRLEN(match_array));
    return result;
}


typedef struct {
    const uint8_t *data;
    int64_t len;
} AOC_String;

#define AOC_STRLIT(s) ((AOC_String){(const uint8_t *)s,sizeof(s)-1})
#define AOC_STRVARG(s) ((int)(s).len), ((s).data)

bool
AOC_String_Match(AOC_String s1, AOC_String s2) {
    if (s1.len != s2.len) return false;
    for (int64_t cursor = 0; cursor < s1.len; cursor+=1) {
        if (s1.data[cursor] != s2.data[cursor]) return false;
    }
    return true;
}






bool
AOC_Extract_rune(AOC_String s, int64_t cursor, uint32_t *result_rune, int64_t *result_byte_size) {

    uint32_t rune     = 0;
    int64_t byte_size = 0;
    bool success      = false;

    if (cursor < s.len) {
        byte_size = 1;
        uint32_t c0 = s.data[cursor];
        if ((0x80&c0) == 0) { // 1 byte
            rune    = c0;
            success = true;
        }
        else if ((0xE0&c0)==0xC0) {     // 2 bytes
            if (cursor+2 <= s.len) {
                byte_size   = 2;
                uint32_t c1 = s.data[cursor+1];
                if ((c1&0xC0)==0x80) {
                    rune    = ((c0&0x3F)<<6)|(c1&0x3F);
                    success = true;
                }
            }
        }
        else if ((0xF0&c0)==0xE0) {     // 3 bytes
            if (cursor+3 <= s.len) {
                byte_size   = 3;
                uint32_t c1 = s.data[cursor+1];
                uint32_t c2 = s.data[cursor+2];
                if ((c1&0xC0)==0x80 && (c2&0xC0)==0x80) {
                    rune    = ((c0&0x0F)<<12)|((c1&0x3F)<<6)|((c2&0x3F));
                    success = true;
                }
            }
        }
        else if ((0xF8&c0)==0xF0) {    // 4 bytes
            if (cursor+4 <= s.len) {
                byte_size = 4;
                uint32_t c1 = s.data[cursor+1];
                uint32_t c2 = s.data[cursor+2];
                uint32_t c3 = s.data[cursor+3];
                if ((c1&0xC0)==0x80 && (c2&0xC0)==0x80 && (c3&0xC0)==0x80) {
                    rune    = ((c0&0x07)<<18)|((c1&0x3F)<<12)|((c2&0x3F)<<6)|((c3&0x3F));
                    success = true;
                }
            }
        }
    }
    if (result_rune)      *result_rune = rune;
    if (result_byte_size) *result_byte_size = byte_size;
    return success;
}


int64_t
AOC_Eat_spaces(AOC_String s, int64_t cursor) {
    for (;;) {
        uint32_t rune;
        int64_t  rune_byte_size;
        if (!AOC_Extract_rune(s, cursor, &rune, &rune_byte_size)) break;
        if (!AOC_Rune_is_whitespace(rune)) break;
        if (rune == 0) break;
        cursor += rune_byte_size;
    }
    return cursor;
}

enum {
    AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION = (1<<0),
    AOC_TOKEN_FLAG_TYPE                       = (1<<1),
    AOC_TOKEN_FLAG_KEYWORD                    = (1<<2),
    AOC_TOKEN_FLAG_EXPRESSION_SYMBOL          = (1<<3),
} AOC_TOKEN_FLAG;

#define AOC_TOKEN_KIND_XMACRO(x) \
    x(AOC_TOKEN_KIND_NULL,             0) \
    x(AOC_TOKEN_KIND_SEMICOLON,        0) \
    x(AOC_TOKEN_KIND_OPEN_PAREN,       0) \
    x(AOC_TOKEN_KIND_CLOSE_PAREN,      0) \
    x(AOC_TOKEN_KIND_OPEN_CURLYBRACE,  0) \
    x(AOC_TOKEN_KIND_CLOSE_CURLYBRACE, 0) \
    x(AOC_TOKEN_KIND_OPEN_BRACKET,     0) \
    x(AOC_TOKEN_KIND_CLOSE_BRACKET,    0) \
    x(AOC_TOKEN_KIND_COMMA,            0) \
    x(AOC_TOKEN_KIND_PLUS,             AOC_TOKEN_FLAG_EXPRESSION_SYMBOL) \
    x(AOC_TOKEN_KIND_HYPEN,            AOC_TOKEN_FLAG_EXPRESSION_SYMBOL) \
    x(AOC_TOKEN_KIND_EQUAL,            AOC_TOKEN_FLAG_EXPRESSION_SYMBOL) \
    x(AOC_TOKEN_KIND_NUMBER,           AOC_TOKEN_FLAG_EXPRESSION_SYMBOL) \
    x(AOC_TOKEN_KIND_IDENTIFIER,       AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION)

#define AOC_TOKEN_KIND_KEYWORD_XMACRO(x) \
    x(AOC_TOKEN_KIND_KEYWORD_AUTO,     AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "auto"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_DOUBLE,   AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "double"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_INT,      AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "int"     ) \
    x(AOC_TOKEN_KIND_KEYWORD_STRUCT,   AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "struct"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_BREAK,    AOC_TOKEN_FLAG_KEYWORD|0,                                                             "break"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_ELSE,     AOC_TOKEN_FLAG_KEYWORD|0,                                                             "else"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_LONG,     AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "long"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_SWITH,    AOC_TOKEN_FLAG_KEYWORD|0,                                                             "swith"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_CASE,     AOC_TOKEN_FLAG_KEYWORD|0,                                                             "case"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_ENUM,     AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "enum"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_REGISTER, AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "register") \
    x(AOC_TOKEN_KIND_KEYWORD_TYPEDEF,  AOC_TOKEN_FLAG_KEYWORD|0,                                                             "typedef" ) \
    x(AOC_TOKEN_KIND_KEYWORD_CHAR,     AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "char"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_EXTERN,   AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "extern"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_RETURN,   AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "return"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_UNION,    AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "union"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_CONST,    AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "const"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_FLOAT,    AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "float"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_SHORT,    AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "short"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_UNSIGNED, AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "unsigned") \
    x(AOC_TOKEN_KIND_KEYWORD_CONTINUE, AOC_TOKEN_FLAG_KEYWORD|0,                                                             "continue") \
    x(AOC_TOKEN_KIND_KEYWORD_FOR,      AOC_TOKEN_FLAG_KEYWORD|0,                                                             "for"     ) \
    x(AOC_TOKEN_KIND_KEYWORD_SIGNED,   AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "signed"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_VOID,     AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "void"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_DEFAULT,  AOC_TOKEN_FLAG_KEYWORD|0,                                                             "default" ) \
    x(AOC_TOKEN_KIND_KEYWORD_GOTO,     AOC_TOKEN_FLAG_KEYWORD|0,                                                             "goto"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_SIZEOF,   AOC_TOKEN_FLAG_KEYWORD|0,                                                             "sizeof"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_VOLATILE, AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "volatile") \
    x(AOC_TOKEN_KIND_KEYWORD_DO,       AOC_TOKEN_FLAG_KEYWORD|0,                                                             "do"      ) \
    x(AOC_TOKEN_KIND_KEYWORD_IF,       AOC_TOKEN_FLAG_KEYWORD|0,                                                             "if"      ) \
    x(AOC_TOKEN_KIND_KEYWORD_STATIC,   AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "static"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_WHILE,    AOC_TOKEN_FLAG_KEYWORD|0,                                                             "while"   )


typedef enum {
    #define x(token_kind, flags) token_kind,
    AOC_TOKEN_KIND_XMACRO(x)
    #undef x
    #define x(token_kind, flags, keyword_str) token_kind,
    AOC_TOKEN_KIND_KEYWORD_XMACRO(x)
    #undef x
} AOC_TokenKind;

AOC_String AOC_TokenKind_to_string[] = {
    #define x(token_kind, flags) [token_kind] = AOC_STRLIT(# token_kind),
    AOC_TOKEN_KIND_XMACRO(x)
    #undef x
    #define x(token_kind, flags, keyword_str) [token_kind] = AOC_STRLIT(# token_kind),
    AOC_TOKEN_KIND_KEYWORD_XMACRO(x)
    #undef x
};

uint64_t AOC_TokenKind_to_flags[] = {
    #define x(token_kind, flags) [token_kind] = flags,
    AOC_TOKEN_KIND_XMACRO(x)
    #undef x
    #define x(token_kind, flags, keyword_str) [token_kind] = flags,
    AOC_TOKEN_KIND_KEYWORD_XMACRO(x)
    #undef x
};

typedef struct {
    int64_t    kind;
    AOC_String string;
} AOC_TokenKeywordStringPair;

AOC_TokenKeywordStringPair AOC_token_keyword_string_table[] = {
    #define x(token_kind, flags, keyword_str) {token_kind, AOC_STRLIT(keyword_str)},
    AOC_TOKEN_KIND_KEYWORD_XMACRO(x)
    #undef x
};

bool
AOC_String_Is_keyword(AOC_String s, int64_t *token_keyword_kind) {
    for (int64_t i = 0; i < AOC_ARRLEN(AOC_token_keyword_string_table); i+=1) {
        if (AOC_String_Match(s, AOC_token_keyword_string_table[i].string)) {
            if (token_keyword_kind) *token_keyword_kind = AOC_token_keyword_string_table[i].kind;
            return true;
        }
    }
    return false;
}


typedef struct {
    AOC_String filename;
    int64_t line;
    int64_t column;
} AOC_FileCoordinates;

#define AOC_COORDFMT  "%.*s:%lu:%lu"
#define AOC_COORDVARG(coords) AOC_STRVARG((coords).filename), (coords).line, (coords).column

typedef struct AOC_Token AOC_Token;
struct AOC_Token {
    AOC_FileCoordinates coordinates;
    AOC_Token *next;
    AOC_String string;
    AOC_TokenKind kind;
};


typedef struct {
    AOC_String s;
    int64_t cursor;
    AOC_FileCoordinates coordinates;
} AOC_Tokenizer;


void
AOC_Print_token(AOC_Token token) {
    AOC_String kind_str = AOC_TokenKind_to_string[token.kind];
    printf(AOC_COORDFMT": Kind: %d \"%.*s\" String: \"%.*s\"", AOC_COORDVARG(token.coordinates), token.kind, AOC_STRVARG(kind_str), AOC_STRVARG(token.string));
}

void
AOC_Print_error(AOC_FileCoordinates coords, const char *fmt, ...) {
    printf(AOC_COORDFMT": Error: ", AOC_COORDVARG(coords));
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
    puts("");
}

bool
AOC_Next_token(AOC_Tokenizer *tokenizer, AOC_Token *token_result) {

    // Eat spaces
    for (;;) {
        uint32_t rune;
        int64_t  rune_byte_size;
        if (!AOC_Extract_rune(tokenizer->s, tokenizer->cursor, &rune, &rune_byte_size)) break;
        if (rune == 0) break;
        if (rune == ' ' || rune == '\t') {
            tokenizer->coordinates.column += 1;
        }
        else if (rune == '\n') {
            tokenizer->coordinates.line += 1;
            tokenizer->coordinates.column = 1;
        }
        else if (rune != '\r') {
            break;
        }
        tokenizer->cursor += rune_byte_size;
    }

    bool       error        = false;
    int64_t    token_kind   = AOC_TOKEN_KIND_NULL;
    AOC_String token_string = AOC_STRLIT("");
    AOC_FileCoordinates token_coordinates = {0};

    if (tokenizer->cursor >= tokenizer->s.len) error = true;
    
    // Extract the first rune
    uint32_t rune;
    int64_t  rune_byte_size;
    if (!error) {
        if (!AOC_Extract_rune(tokenizer->s, tokenizer->cursor, &rune, &rune_byte_size)) {
            error = true;
        }
    }
    if (!error) {
        token_string.data = &(tokenizer->s.data[tokenizer->cursor]);
        token_string.len  = rune_byte_size;
        token_coordinates = tokenizer->coordinates;
        tokenizer->cursor += rune_byte_size;
        tokenizer->coordinates.column += 1;
        if      (rune == ';') token_kind = AOC_TOKEN_KIND_SEMICOLON;
        else if (rune == '(') token_kind = AOC_TOKEN_KIND_OPEN_PAREN;
        else if (rune == ')') token_kind = AOC_TOKEN_KIND_CLOSE_PAREN;
        else if (rune == '{') token_kind = AOC_TOKEN_KIND_OPEN_CURLYBRACE;
        else if (rune == '}') token_kind = AOC_TOKEN_KIND_CLOSE_CURLYBRACE;
        else if (rune == '[') token_kind = AOC_TOKEN_KIND_OPEN_BRACKET;
        else if (rune == ']') token_kind = AOC_TOKEN_KIND_CLOSE_BRACKET;
        else if (rune == ',') token_kind = AOC_TOKEN_KIND_COMMA;
        else if (rune == '+') token_kind = AOC_TOKEN_KIND_PLUS;
        else if (rune == '-') token_kind = AOC_TOKEN_KIND_HYPEN;
        else if (rune == '=') token_kind = AOC_TOKEN_KIND_EQUAL;
        else if (AOC_Rune_is_numeric(rune)) {
            token_kind = AOC_TOKEN_KIND_NUMBER;
            for (;;) {
                if (!AOC_Extract_rune(tokenizer->s, tokenizer->cursor, &rune, &rune_byte_size)) break;
                if (AOC_Rune_is_symbol(rune)||AOC_Rune_is_whitespace(rune)) break;
                tokenizer->cursor += rune_byte_size;
                tokenizer->coordinates.column += 1;
                token_string.len += rune_byte_size;
            }
        }
        else {
            token_kind = AOC_TOKEN_KIND_IDENTIFIER;
            for (;;) {
                if (!AOC_Extract_rune(tokenizer->s, tokenizer->cursor, &rune, &rune_byte_size)) break;
                if (AOC_Rune_is_symbol(rune)||AOC_Rune_is_whitespace(rune)) break;
                tokenizer->cursor += rune_byte_size;
                tokenizer->coordinates.column += 1;
                token_string.len  += rune_byte_size;
            }
            int64_t token_keyword_kind;
            if (AOC_String_Is_keyword(token_string, &token_keyword_kind)) {
                token_kind = token_keyword_kind;
            }
            else {
                token_kind = AOC_TOKEN_KIND_IDENTIFIER;
            }
        }
    }
    if (token_result) {
        token_result->kind   = token_kind;
        token_result->string = token_string;
        token_result->coordinates = token_coordinates;
    }
    return !error;
}


AOC_Token *
AOC_Tokenize(AOC_Arena *arena, AOC_String s) {
    AOC_Tokenizer tokenizer = {.s = s, .cursor = 0, .coordinates = {.filename = AOC_STRLIT(""), .line = 1, .column = 1}};
    AOC_Token *result     = NULL;
    AOC_Token *last_token = NULL;
    AOC_Token token;
    for (;;) {
        AOC_Next_token(&tokenizer, &token);
        AOC_Token *allocated_token = AOC_ARENA_NEW(arena, AOC_Token);
        *allocated_token = token;
        allocated_token->next = NULL;
        if (!result)    result           = allocated_token;
        if (last_token) last_token->next = allocated_token;
        last_token = allocated_token;
        if (token.kind == AOC_TOKEN_KIND_NULL) break;
    }
    return result;
}


#define AOC_AST_NODE_KIND_XMACRO(x)  \
    x(AOC_AST_NODE_KIND_NULL)        \
    x(AOC_AST_NODE_KIND_FUNCTION)    \
    x(AOC_AST_NODE_KIND_DECLARATION) \
    x(AOC_AST_NODE_KIND_EXPRESION)   \
    x(AOC_AST_NODE_KIND_TYPE)        \
    x(AOC_AST_NODE_KIND_IDENTIFIER)  \
    x(AOC_AST_NODE_KIND_VALUE)


enum {
    #define x(kind) kind,
    AOC_AST_NODE_KIND_XMACRO(x)
    #undef x
};



typedef struct AOC_AstNode AOC_AstNode;
struct AOC_AstNode {
    AOC_AstNode *next;
    int64_t kind;
    union {
        struct {
            AOC_AstNode *child;
        } type;
        struct {
            AOC_String name;
            AOC_AstNode *return_node;
            AOC_AstNode *parameters_node;
            AOC_AstNode *body;
        } function;
        struct {
            AOC_String name;
            AOC_AstNode *type_node;
            AOC_AstNode *assignment;
        } declaration;
    };
};




AOC_AstNode *
AOC_Parse_type(AOC_Arena *arena, AOC_Token *token_type, AOC_Token **last_type_token) {
    AOC_AstNode *result   = AOC_ARENA_NEW_ZERO(arena, AOC_AstNode);
    result->kind          = AOC_AST_NODE_KIND_TYPE;
    token_type            = token_type->next;
    uint64_t token_flags = AOC_TokenKind_to_flags[token_type->kind];
    if (token_flags & AOC_TOKEN_FLAG_TYPE) {
        result->type.child = AOC_Parse_type(arena, token_type, &token_type);
    }
    if (last_type_token) *last_type_token = token_type;
    return result;
}


AOC_AstNode *
AOC_Parse_function_parameters(AOC_Arena *arena, AOC_Token *token_node, AOC_Token **last_token) {
    AOC_AstNode *result    = NULL;
    AOC_AstNode *last_node = NULL;
    bool expect_comma = false;
    for (;;) {
        if (token_node->kind == AOC_TOKEN_KIND_CLOSE_PAREN) {
            token_node = token_node->next;
            break;
        }

        // Comma handling
        if (expect_comma) {
            if (token_node->kind != AOC_TOKEN_KIND_COMMA) {
                AOC_Print_error(token_node->coordinates, "expected \",\" or \")\" but got \"%.*s\"\n", AOC_STRVARG(token_node->string));
                return NULL;
            }
            token_node = token_node->next;
        }
        expect_comma = true;

        uint64_t token_flags = AOC_TokenKind_to_flags[token_node->kind];

        AOC_AstNode *ast_node_type = NULL;
        if (token_flags & AOC_TOKEN_FLAG_TYPE) {
            ast_node_type = AOC_Parse_type(arena, token_node, &token_node);
        }
        AOC_AstNode *ast_node_declaration = NULL;
        if (token_node->kind == AOC_TOKEN_KIND_IDENTIFIER) {
            ast_node_declaration = AOC_ARENA_NEW_ZERO(arena, AOC_AstNode);
            ast_node_declaration->kind = AOC_AST_NODE_KIND_DECLARATION;
            ast_node_declaration->declaration.name = token_node->string;
            ast_node_declaration->declaration.type_node = ast_node_type;
            token_node = token_node->next;
        }
        AOC_AstNode *param_node = NULL;
        if (ast_node_declaration) {
            param_node = ast_node_declaration;
        }
        else if (ast_node_type) {
            param_node = ast_node_type;
        }
        else {
            printf("Error, unexpected token %.*s in a parameter list\n", AOC_STRVARG(token_node->string));
            return NULL;
        }

        if (!result) result = param_node;
        if (last_node) last_node->next = param_node;
        last_node = param_node;
    }
    if (last_token) *last_token = token_node;
    return result;
}


AOC_AstNode *
AOC_Parse_expression(AOC_Arena *arena, AOC_Token *token_node, AOC_Token **last_token) {
    for (;;) {
        if (token_node->kind == AOC_TOKEN_KIND_NULL) break;
        if (token_node->kind == AOC_TOKEN_KIND_SEMICOLON) break;
        token_node = token_node->next;
    }
    if (last_token) *last_token = token_node;
    return NULL;
}

AOC_AstNode *
AOC_Parse(AOC_Arena *arena, bool inside_body, AOC_Token *token_node, AOC_Token **last_token);

AOC_AstNode *
AOC_Parse_function(AOC_Arena *arena, AOC_Token *token_node, AOC_Token **last_token) {
    AOC_AstNode *ast_node_params   = AOC_Parse_function_parameters(arena, token_node->next, &token_node);
    AOC_AstNode *ast_node_body = NULL;
    if (token_node->kind == AOC_TOKEN_KIND_OPEN_CURLYBRACE) {
        token_node = token_node->next;
        ast_node_body = AOC_Parse(arena, true, token_node, &token_node);
    }
    else if (token_node->kind != AOC_TOKEN_KIND_SEMICOLON) {
        AOC_Print_error(token_node->coordinates, "expected \";\" but got \"%.*s\"", AOC_STRVARG(token_node->string));
    }
    AOC_AstNode *function_ast_node = AOC_ARENA_NEW_ZERO(arena, AOC_AstNode);
    function_ast_node->kind = AOC_AST_NODE_KIND_FUNCTION;
    function_ast_node->function.parameters_node = ast_node_params;
    function_ast_node->function.body= ast_node_body;
    if (last_token) *last_token = token_node;
    return function_ast_node;
}


AOC_AstNode *
AOC_Parse_value_decl(AOC_Arena *arena, AOC_Token *token_node, AOC_Token **last_token) {
    AOC_AstNode *decl_ast_node = AOC_ARENA_NEW_ZERO(arena, AOC_AstNode);
    decl_ast_node->kind = AOC_AST_NODE_KIND_DECLARATION;
    if  (token_node->kind == AOC_TOKEN_KIND_EQUAL) {
        token_node = token_node->next;
        decl_ast_node->declaration.assignment = AOC_Parse_expression(arena, token_node, &token_node);
    }
    else if (token_node->kind != AOC_TOKEN_KIND_SEMICOLON) {
        AOC_Print_error(token_node->coordinates, "expected \";\" or \"=\" but got \"%.*s\"", AOC_STRVARG(token_node->string));
    }
    if (last_token) *last_token = token_node;
    return decl_ast_node;
}


AOC_AstNode *
AOC_Parse(AOC_Arena *arena, bool inside_body, AOC_Token *token_node, AOC_Token **last_token) {
    AOC_AstNode *result    = NULL;
    AOC_AstNode *last_node = NULL;
    for (;;) {
        if (inside_body) {
            if (token_node->kind == AOC_TOKEN_KIND_CLOSE_CURLYBRACE) {
                token_node = token_node->next;
                break;
            }
            else if (token_node->kind == AOC_TOKEN_KIND_NULL) {
                AOC_Print_error(token_node->coordinates, "expected \"}\" but got end of file.");
                break;
            }
        }
        else if (token_node->kind == AOC_TOKEN_KIND_NULL) {
            break;
        }
        uint64_t token_flags = AOC_TokenKind_to_flags[token_node->kind];
        AOC_AstNode *new_ast_node = NULL;
        if (token_flags & AOC_TOKEN_FLAG_TYPE) {
            AOC_AstNode *ast_node_type = AOC_Parse_type(arena, token_node, &token_node);
            if (token_node->kind == AOC_TOKEN_KIND_IDENTIFIER) {
                AOC_Token *token_identifier = token_node;
                token_node=token_node->next;
                if (token_node->kind == AOC_TOKEN_KIND_OPEN_PAREN) {
                    new_ast_node = AOC_Parse_function(arena, token_node, &token_node);
                    new_ast_node->function.name            = token_identifier->string;
                    new_ast_node->function.return_node     = ast_node_type;
                }
                else {
                    new_ast_node = AOC_Parse_value_decl(arena, token_node, &token_node);
                    new_ast_node->declaration.name      = token_identifier->string;
                    new_ast_node->declaration.type_node = ast_node_type;
                }
            }
        }
        else {
            token_node = token_node->next;
        }
        if (new_ast_node) {
            if (!result) result = new_ast_node;
            if (last_node) last_node->next = new_ast_node;
            last_node = new_ast_node;
        }
    }
    if (last_token) *last_token = token_node;

    return result;
}


void
AOC_Print_ast(AOC_AstNode *ast_node, int64_t ident) {
    if (!ast_node) {
        return;
    }
    for (;ast_node; ast_node=ast_node->next) {
        if      (ast_node->kind == AOC_AST_NODE_KIND_FUNCTION) {
            printf("%*cFUNCTION: %.*s\n", (int)ident, ' ', AOC_STRVARG(ast_node->function.name));
            printf("%*cRETURN\n", (int)ident+2, ' ');
            AOC_Print_ast(ast_node->function.return_node, ident+4);
            printf("%*cPARAMETERS\n", (int)ident+2, ' ');
            AOC_Print_ast(ast_node->function.parameters_node, ident+4);
            printf("%*cBODY: \n", (int)ident+2, ' ');
            AOC_Print_ast(ast_node->function.body, ident+4);
        }
        else if (ast_node->kind == AOC_AST_NODE_KIND_DECLARATION) {
            printf("%*cDECLARATION: %.*s\n", (int)ident, ' ', AOC_STRVARG(ast_node->declaration.name));
            AOC_Print_ast(ast_node->declaration.type_node, ident+2);
            
        }
        else if (ast_node->kind == AOC_AST_NODE_KIND_TYPE) {
            printf("%*cTYPE\n", (int)ident, ' ');
            AOC_Print_ast(ast_node->type.child, ident+4);
        }
    }
}



#ifndef TESTS



int
main(void) {
    AOC_String input = AOC_STRLIT(
    "int add5(int x) {\n"
    "   int result = x+5;\n"
    "   return result;\n"
    "}\n"
    "int add(int a, int b) {\n"
    "   int result = a+b;\n"
    "   return result;\n"
    "}\n"
    "int no_params();\n"
    "int no_identifiers(float, int);\n"
    "\n"
    "int square(int x) {\n"
    "   int result = x*x;\n"
    "   return result;\n"
    "}\n");
    AOC_Arena *arena = AOC_Arena_Allocate();
    AOC_Token *token_node = AOC_Tokenize(arena, input);
    for (AOC_Token *t = token_node; t; t=t->next) {
        AOC_Print_token(*t);
        printf("\n");
    }
    AOC_AstNode *ast_node = AOC_Parse(arena, false, token_node, NULL);
    AOC_Print_ast(ast_node, 0);
    return 0;
}

#else

#include "utest.h"

UTEST(utf8, utf8) {
    {
        AOC_String s = AOC_STRLIT("$");
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(s, 0, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0x24);
        ASSERT_EQ(rune_byte_size, 1);
    }
    {
        AOC_String s = AOC_STRLIT("£");
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(s, 0, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0xA3);
        ASSERT_EQ(rune_byte_size, 2);
    }
    {
        AOC_String s = AOC_STRLIT("ह");
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(s, 0, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0x939);
        ASSERT_EQ(rune_byte_size, 3);
    }
    {
        AOC_String s = AOC_STRLIT("€");
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(s, 0, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0x20AC);
        ASSERT_EQ(rune_byte_size, 3);
    }
    {
        AOC_String s = AOC_STRLIT("한");
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(s, 0, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0xD55C);
        ASSERT_EQ(rune_byte_size, 3);
    }
    {
        AOC_String s = AOC_STRLIT("𐍈");
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(s, 0, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0x10348);
        ASSERT_EQ(rune_byte_size, 4);
    }
    {
        uint8_t data[] = {0xF0,0xFF,0xFF,0xFF};
        AOC_String s;
        s.data = data;
        s.len  = sizeof(data);
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_FALSE(AOC_Extract_rune(s, 0, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0);
        ASSERT_EQ(rune_byte_size, 4);
    }
}


UTEST(arena, arena) {
    AOC_Arena *arena1 = AOC_Arena_Allocate();

    AOC_Arena_Push_aligned(arena1, 1024, 1);
    ASSERT_EQ(arena1->current->cursor, 1024);
    AOC_Arena_Push_aligned(arena1, AOC_ARENA_CAPACITY, 1);
    ASSERT_EQ(arena1->current->cursor, AOC_ARENA_CAPACITY);
    ASSERT_TRUE(arena1->current->prev != NULL);
    
    AOC_Arena_Release(arena1);
    ASSERT_TRUE(AOC_free_arenas       != NULL);
    ASSERT_TRUE(AOC_free_arenas->prev != NULL);
    ASSERT_TRUE(AOC_free_arenas->prev->prev == NULL);

    AOC_Arena *arena2 = AOC_Arena_Allocate();
    AOC_Arena *arena3 = AOC_Arena_Allocate();
    ASSERT_TRUE(AOC_free_arenas == NULL);
}

UTEST_MAIN();

#endif


