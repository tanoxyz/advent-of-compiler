
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>


#define AOC_ARENA_CAPACITY (4*1024*1024) // 4mb
typedef struct AOC_Arena AOC_Arena;
struct AOC_Arena {
    AOC_Arena *prev;
    AOC_Arena *current;
    int64_t cursor;
    uint8_t data[AOC_ARENA_CAPACITY];
};

AOC_Arena *AOC_free_arenas = NULL;

AOC_Arena *
AOC_Arena_Allocate(void) {
    AOC_Arena *result = NULL;
    if (AOC_free_arenas) {
        result = AOC_free_arenas;
        AOC_free_arenas = AOC_free_arenas->prev;
    }
    else {
        uint8_t *allocated_data = sbrk(sizeof(AOC_Arena)+(sizeof(void*)-1));
        uintptr_t allocated_data_int = (uintptr_t)allocated_data;
        allocated_data_int = (allocated_data_int+(sizeof(void*)-1))&(~(sizeof(void*)-1)); // Align
        result = (AOC_Arena *)allocated_data_int;
    }
    result->prev = NULL;
    result->current = result;
    result->cursor = 0;
    return result;
}

void
AOC_Arena_Release(AOC_Arena *arena) {
    arena->prev = AOC_free_arenas;
    AOC_free_arenas = arena->current;
}

void *
AOC_Arena_Push_aligned(AOC_Arena *arena, int64_t total_bytes, int64_t alignement) {
    AOC_Arena *arena_curr   = arena->current;
    uintptr_t start_address = (uintptr_t)(arena_curr->data+arena_curr->cursor);
    uintptr_t end_address   = (uintptr_t)(arena_curr->data+AOC_ARENA_CAPACITY);
    uintptr_t start_address_aligned = (start_address+(alignement-1))-((start_address+(alignement-1))%alignement);
    arena_curr->cursor     += (start_address_aligned+total_bytes)-start_address;
    void *result = (void *)start_address_aligned;

    // Not enough memory in the current arena, allocate a new arena
    if (start_address_aligned+total_bytes > end_address) {
        AOC_Arena *arena_new = AOC_Arena_Allocate();
        start_address = (uintptr_t)(arena_new->data);
        end_address   = (uintptr_t)(arena_new->data+AOC_ARENA_CAPACITY);
        start_address_aligned = (start_address+(alignement-1))-((start_address+(alignement-1))%alignement);

        if (start_address_aligned+total_bytes > end_address) {
            result = NULL; // Didn't fit anyway
        }
        else {
            arena_new->cursor    = (start_address_aligned+total_bytes)-start_address;
            result = (void *)start_address_aligned;
        }

        arena_new->prev = arena_curr;
        arena->current  = arena_new;
    }

    return result;
}


#define AOC_ARENA_NEW(arena, T) ((T*)AOC_Arena_Push_aligned(arena, sizeof(T), sizeof(void*)))


#define AOC_ARRLEN(a) (sizeof(a)/sizeof(a[0]))

bool
AOC_Rune_match(uint32_t rune, uint32_t match_array[], int64_t match_array_count) {
    for (int64_t i=0; i < match_array_count; i+=1) {
        if (rune == match_array[i]) return true;
    }
    return false;
}

bool
AOC_Rune_is_whitespace(uint32_t rune) {
    static uint32_t match_array[] = {' ', '\n', '\r', '\t'};
    bool result = AOC_Rune_match(rune, match_array, AOC_ARRLEN(match_array));
    return result;
}

bool
AOC_Rune_is_alpha(uint32_t rune) {
    static uint32_t match_array[] = {
        'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v',
        'w','x','y','z', 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R',
        'S','T','U','V','W','X','Y','Z'
    };
    bool result = AOC_Rune_match(rune, match_array, AOC_ARRLEN(match_array));
    return result;
}

bool
AOC_Rune_is_numeric(uint32_t rune) {
    static uint32_t match_array[] = {'1','2','3','4','5','6','7','8','9','0'};
    bool result = AOC_Rune_match(rune, match_array, AOC_ARRLEN(match_array));
    return result;
}

bool
AOC_Rune_is_symbol(uint32_t rune) {
    static uint32_t match_array[] = {
        '/','.',':',';',',','[',']','<','>','!','%','^','&','*','(',')','{','}','-','=','+','|',
        '~','"','\''
    };
    bool result = AOC_Rune_match(rune, match_array, AOC_ARRLEN(match_array));
    return result;
}


typedef struct {
    const uint8_t *data;
    int64_t len;
} AOC_String;

#define AOC_STRLIT(s) ((AOC_String){(const uint8_t *)s,sizeof(s)-1})
#define AOC_STRVARG(s) ((int)(s).len), ((s).data)

bool
AOC_String_Match(AOC_String s1, AOC_String s2) {
    if (s1.len != s2.len) return false;
    for (int64_t cursor = 0; cursor < s1.len; cursor+=1) {
        if (s1.data[cursor] != s2.data[cursor]) return false;
    }
    return true;
}

typedef struct AOC_AstNode AOC_AstNode;

typedef struct {
    const uint8_t *data;
    int64_t data_len;
    int64_t cursor;
    AOC_AstNode *ast;
} AOC_State;



bool
AOC_Extract_rune(AOC_State *state, uint32_t *result_rune, int64_t *result_byte_size) {

    int64_t        data_len = state->data_len;
    int64_t        cursor   = state->cursor;
    const uint8_t *data     = state->data;

    uint32_t rune     = 0;
    int64_t byte_size = 0;
    bool success      = false;

    if (cursor < data_len) {
        byte_size = 1;
        uint32_t c0 = data[cursor];
        if ((0x80&c0) == 0) { // 1 byte
            rune    = c0;
            success = true;
        }
        else if ((0xE0&c0)==0xC0) {     // 2 bytes
            if (cursor+2 <= data_len) {
                byte_size   = 2;
                uint32_t c1 = data[cursor+1];
                if ((c1&0xC0)==0x80) {
                    rune    = ((c0&0x3F)<<6)|(c1&0x3F);
                    success = true;
                }
            }
        }
        else if ((0xF0&c0)==0xE0) {     // 3 bytes
            if (cursor+3 <= data_len) {
                byte_size   = 3;
                uint32_t c1 = data[cursor+1];
                uint32_t c2 = data[cursor+2];
                if ((c1&0xC0)==0x80 && (c2&0xC0)==0x80) {
                    rune    = ((c0&0x0F)<<12)|((c1&0x3F)<<6)|((c2&0x3F));
                    success = true;
                }
            }
        }
        else if ((0xF8&c0)==0xF0) {    // 4 bytes
            if (cursor+4 <= data_len) {
                byte_size = 4;
                uint32_t c1 = data[cursor+1];
                uint32_t c2 = data[cursor+2];
                uint32_t c3 = data[cursor+3];
                if ((c1&0xC0)==0x80 && (c2&0xC0)==0x80 && (c3&0xC0)==0x80) {
                    rune    = ((c0&0x07)<<18)|((c1&0x3F)<<12)|((c2&0x3F)<<6)|((c3&0x3F));
                    success = true;
                }
            }
        }
    }
    if (result_rune)      *result_rune = rune;
    if (result_byte_size) *result_byte_size = byte_size;
    return success;
}


void
AOC_Eat_spaces(AOC_State *state) {
    for (;;) {
        uint32_t rune;
        int64_t  rune_byte_size;
        if (!AOC_Extract_rune(state, &rune, &rune_byte_size)) return;
        if (!AOC_Rune_is_whitespace(rune)) return;
        if (rune == 0) return;
        state->cursor += rune_byte_size;
    }
}

enum {
    AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION = (1<<0),
    AOC_TOKEN_FLAG_TYPE                       = (1<<1),
} AOC_TOKEN_FLAG;

#define AOC_TOKEN_KIND_XMACRO(x) \
    x(AOC_TOKEN_KIND_NULL,             0) \
    x(AOC_TOKEN_KIND_SEMICOLON,        0) \
    x(AOC_TOKEN_KIND_OPEN_PAREN,       0) \
    x(AOC_TOKEN_KIND_CLOSE_PAREN,      0) \
    x(AOC_TOKEN_KIND_OPEN_CURLYBRACE,  0) \
    x(AOC_TOKEN_KIND_CLOSE_CURLYBRACE, 0) \
    x(AOC_TOKEN_KIND_COMMA,            0) \
    x(AOC_TOKEN_KIND_PLUS,             0) \
    x(AOC_TOKEN_KIND_EQUAL,            0) \
    x(AOC_TOKEN_KIND_NUMBER,           0) \
    x(AOC_TOKEN_KIND_IDENTIFIER,       AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION)

#define AOC_TOKEN_KIND_KEYWORD_XMACRO(x) \
    x(AOC_TOKEN_KIND_KEYWORD_AUTO,     AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "auto"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_DOUBLE,   AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "double"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_INT,      AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "int"     ) \
    x(AOC_TOKEN_KIND_KEYWORD_STRUCT,   AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "struct"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_BREAK,    0,                                                             "break"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_ELSE,     0,                                                             "else"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_LONG,     AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "long"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_SWITH,    0,                                                             "swith"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_CASE,     0,                                                             "case"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_ENUM,     AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "enum"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_REGISTER, AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "register") \
    x(AOC_TOKEN_KIND_KEYWORD_TYPEDEF,  0,                                                             "typedef" ) \
    x(AOC_TOKEN_KIND_KEYWORD_CHAR,     AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "char"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_EXTERN,   AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "extern"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_RETURN,   AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "return"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_UNION,    AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "union"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_CONST,    AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "const"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_FLOAT,    AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "float"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_SHORT,    AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "short"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_UNSIGNED, AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "unsigned") \
    x(AOC_TOKEN_KIND_KEYWORD_CONTINUE, 0,                                                             "continue") \
    x(AOC_TOKEN_KIND_KEYWORD_FOR,      0,                                                             "for"     ) \
    x(AOC_TOKEN_KIND_KEYWORD_SIGNED,   AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "signed"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_VOID,     AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "void"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_DEFAULT,  0,                                                             "default" ) \
    x(AOC_TOKEN_KIND_KEYWORD_GOTO,     0,                                                             "goto"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_SIZEOF,   0,                                                             "sizeof"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_VOLATILE, AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "volatile") \
    x(AOC_TOKEN_KIND_KEYWORD_DO,       0,                                                             "do"      ) \
    x(AOC_TOKEN_KIND_KEYWORD_IF,       0,                                                             "if"      ) \
    x(AOC_TOKEN_KIND_KEYWORD_STATIC,   AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "static"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_WHILE,    0,                                                             "while"   )


typedef enum {
    #define x(token_kind, flags) token_kind,
    AOC_TOKEN_KIND_XMACRO(x)
    #undef x
    #define x(token_kind, flags, keyword_str) token_kind,
    AOC_TOKEN_KIND_KEYWORD_XMACRO(x)
    #undef x
} AOC_TokenKind;

AOC_String AOC_TokenKind_to_string[] = {
    #define x(token_kind, flags) [token_kind] = AOC_STRLIT(# token_kind),
    AOC_TOKEN_KIND_XMACRO(x)
    #undef x
    #define x(token_kind, flags, keyword_str) [token_kind] = AOC_STRLIT(# token_kind),
    AOC_TOKEN_KIND_KEYWORD_XMACRO(x)
    #undef x
};

uint64_t AOC_TokenKind_to_flags[] = {
    #define x(token_kind, flags) [token_kind] = flags,
    AOC_TOKEN_KIND_XMACRO(x)
    #undef x
    #define x(token_kind, flags, keyword_str) [token_kind] = flags,
    AOC_TOKEN_KIND_KEYWORD_XMACRO(x)
    #undef x
};

typedef struct {
    int64_t    kind;
    AOC_String string;
} AOC_TokenKeywordStringPair;

AOC_TokenKeywordStringPair AOC_token_keyword_string_table[] = {
    #define x(token_kind, flags, keyword_str) {token_kind, AOC_STRLIT(keyword_str)},
    AOC_TOKEN_KIND_KEYWORD_XMACRO(x)
    #undef x
};

bool
AOC_String_Is_keyword(AOC_String s, int64_t *token_keyword_kind) {
    for (int64_t i = 0; i < AOC_ARRLEN(AOC_token_keyword_string_table); i+=1) {
        if (AOC_String_Match(s, AOC_token_keyword_string_table[i].string)) {
            if (token_keyword_kind) *token_keyword_kind = AOC_token_keyword_string_table[i].kind;
            return true;
        }
    }
    return false;
}

typedef struct {
    AOC_String string;
    AOC_TokenKind kind;
} AOC_Token;

void
AOC_Print_token(AOC_Token token) {
    AOC_String kind_str = AOC_TokenKind_to_string[token.kind];
    printf("Kind: %d \"%.*s\" String: \"%.*s\"", token.kind, AOC_STRVARG(kind_str), AOC_STRVARG(token.string));
}

bool
AOC_Next_token(AOC_State *state, AOC_Token *token_result) {

    AOC_Eat_spaces(state);

    bool       error        = false;
    int64_t    token_kind   = AOC_TOKEN_KIND_NULL;
    AOC_String token_string = AOC_STRLIT("");

    if (state->cursor >= state->data_len) error = true;
    
    // Extract the first rune
    uint32_t rune;
    int64_t  rune_byte_size;
    if (!error) {
        if (!AOC_Extract_rune(state, &rune, &rune_byte_size)) {
            error = true;
        }
    }
    if (!error) {
        token_string.data = &(state->data[state->cursor]);
        token_string.len  = rune_byte_size;
        state->cursor     += rune_byte_size;
        if      (rune == ';') token_kind = AOC_TOKEN_KIND_SEMICOLON;
        else if (rune == '(') token_kind = AOC_TOKEN_KIND_OPEN_PAREN;
        else if (rune == ')') token_kind = AOC_TOKEN_KIND_CLOSE_PAREN;
        else if (rune == '{') token_kind = AOC_TOKEN_KIND_OPEN_CURLYBRACE;
        else if (rune == '}') token_kind = AOC_TOKEN_KIND_CLOSE_CURLYBRACE;
        else if (rune == ',') token_kind = AOC_TOKEN_KIND_COMMA;
        else if (rune == '+') token_kind = AOC_TOKEN_KIND_PLUS;
        else if (rune == '-') token_kind = AOC_TOKEN_KIND_EQUAL;
        else if (AOC_Rune_is_numeric(rune)) {
            token_kind = AOC_TOKEN_KIND_NUMBER;
            for (;;) {
                if (!AOC_Extract_rune(state, &rune, &rune_byte_size)) break;
                if (AOC_Rune_is_symbol(rune)||AOC_Rune_is_whitespace(rune)) break;
                state->cursor    += rune_byte_size;
                token_string.len += rune_byte_size;
            }
        }
        else {
            token_kind = AOC_TOKEN_KIND_IDENTIFIER;
            for (;;) {
                if (!AOC_Extract_rune(state, &rune, &rune_byte_size)) break;
                if (AOC_Rune_is_symbol(rune)||AOC_Rune_is_whitespace(rune)) break;
                state->cursor    += rune_byte_size;
                token_string.len += rune_byte_size;
            }
            int64_t token_keyword_kind;
            if (AOC_String_Is_keyword(token_string, &token_keyword_kind)) {
                token_kind = token_keyword_kind;
            }
            else {
                token_kind = AOC_TOKEN_KIND_IDENTIFIER;
            }
        }
    }
    if (token_result) {
        token_result->kind   = token_kind;
        token_result->string = token_string;
    }
    return !error;
}


#define AOC_AST_NODE_KIND_XMACRO(x)  \
    x(AOC_AST_NODE_KIND_NULL)        \
    x(AOC_AST_NODE_KIND_FUNCTION)    \
    x(AOC_AST_NODE_KIND_DECLARATION) \
    x(AOC_AST_NODE_KIND_EXPRESION)   \
    x(AOC_AST_NODE_KIND_IDENTIFIER)  \
    x(AOC_AST_NODE_KIND_VALUE)

typedef struct AOC_AstNode AOC_AstNode;
struct AOC_AstNode {
    AOC_AstNode *next;
    int64_t kind;
};

#ifndef TESTS

AOC_AstNode *
AOC_Parse_top_level(AOC_Arena *arena, AOC_State *state) {
    AOC_Token token;
    for (;AOC_Next_token(state, &token);) {
        uint64_t token_flags = AOC_TokenKind_to_flags[token.kind];
        if (token_flags & AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION) {
            if (token_flags & AOC_TOKEN_FLAG_TYPE) {
                printf("TYPE ");
            }
        }
        AOC_Print_token(token);
        printf("\n");
    }
    return NULL;
}

int
main(void) {
    uint8_t data[] = 
    "int add5(int x) {\n"
    "   int result = x+5;\n"
    "   return result;\n"
    "}\n";
    AOC_State s;
    s.data = data;
    s.cursor = 0;
    s.data_len = sizeof(data)-1;
    AOC_Arena *arena = AOC_Arena_Allocate();
    AOC_Parse_top_level(arena, &s);
    return 0;
}

#else

#include "utest.h"

UTEST(utf8, utf8) {
    {
        uint8_t data[] = "$";
        AOC_State s;
        s.data     = data;
        s.cursor   = 0;
        s.data_len = sizeof(data)-1;
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(&s, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0x24);
        ASSERT_EQ(rune_byte_size, 1);
    }
    {
        uint8_t data[] = "£";
        AOC_State s;
        s.data     = data;
        s.cursor   = 0;
        s.data_len = sizeof(data)-1;
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(&s, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0xA3);
        ASSERT_EQ(rune_byte_size, 2);
    }
    {
        uint8_t data[] = "ह";
        AOC_State s;
        s.data     = data;
        s.cursor   = 0;
        s.data_len = sizeof(data)-1;
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(&s, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0x939);
        ASSERT_EQ(rune_byte_size, 3);
    }
    {
        uint8_t data[] = "€";
        AOC_State s;
        s.data     = data;
        s.cursor   = 0;
        s.data_len = sizeof(data)-1;
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(&s, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0x20AC);
        ASSERT_EQ(rune_byte_size, 3);
    }
    {
        uint8_t data[] = "한";
        AOC_State s;
        s.data     = data;
        s.cursor   = 0;
        s.data_len = sizeof(data)-1;
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(&s, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0xD55C);
        ASSERT_EQ(rune_byte_size, 3);
    }
    {
        uint8_t data[] = "𐍈";
        AOC_State s;
        s.data     = data;
        s.cursor   = 0;
        s.data_len = sizeof(data)-1;
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(&s, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0x10348);
        ASSERT_EQ(rune_byte_size, 4);
    }
    {
        uint8_t data[] = {0xF0,0xFF,0xFF,0xFF};
        AOC_State s;
        s.data     = data;
        s.cursor   = 0;
        s.data_len = sizeof(data);
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_FALSE(AOC_Extract_rune(&s, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0);
        ASSERT_EQ(rune_byte_size, 4);
    }
}


UTEST(arena, arena) {
    AOC_Arena *arena1 = AOC_Arena_Allocate();

    AOC_Arena_Push_aligned(arena1, 1024, 1);
    ASSERT_EQ(arena1->current->cursor, 1024);
    AOC_Arena_Push_aligned(arena1, AOC_ARENA_CAPACITY, 1);
    ASSERT_EQ(arena1->current->cursor, AOC_ARENA_CAPACITY);
    ASSERT_TRUE(arena1->current->prev != NULL);
    
    AOC_Arena_Release(arena1);
    ASSERT_TRUE(AOC_free_arenas       != NULL);
    ASSERT_TRUE(AOC_free_arenas->prev != NULL);
    ASSERT_TRUE(AOC_free_arenas->prev->prev == NULL);

    AOC_Arena *arena2 = AOC_Arena_Allocate();
    AOC_Arena *arena3 = AOC_Arena_Allocate();
    ASSERT_TRUE(AOC_free_arenas == NULL);
}

UTEST_MAIN();

#endif


