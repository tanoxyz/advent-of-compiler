#!/bin/sh

build() {
    clang -g aoc.c -o aoc
}

tests() {
    clang -g -DTESTS aoc.c -o aoc_tests
    ./aoc_tests
}

${1-build}



