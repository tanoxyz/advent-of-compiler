#!/bin/sh

build() {
    clang -g -fsanitize=undefined aoc.c -o aoc
}

tests() {
    clang -g -fsanitize=undefined -DTESTS aoc.c -o aoc_tests
    ./aoc_tests
}

${1-build}



