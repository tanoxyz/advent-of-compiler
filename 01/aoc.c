
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#define AOC_ARRLEN(a) (sizeof(a)/sizeof(a[0]))

bool
AOC_Rune_match(uint32_t rune, uint32_t match_array[], int64_t match_array_count) {
    for (int64_t i=0; i < match_array_count; i+=1) {
        if (rune == match_array[i]) return true;
    }
    return false;
}

bool
AOC_Rune_is_whitespace(uint32_t rune) {
    static uint32_t match_array[] = {' ', '\n', '\r', '\t'};
    bool result = AOC_Rune_match(rune, match_array, AOC_ARRLEN(match_array));
    return result;
}

bool
AOC_Rune_is_alpha(uint32_t rune) {
    static uint32_t match_array[] = {
        'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v',
        'w','x','y','z', 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R',
        'S','T','U','V','W','X','Y','Z'
    };
    bool result = AOC_Rune_match(rune, match_array, AOC_ARRLEN(match_array));
    return result;
}

bool
AOC_Rune_is_symbol(uint32_t rune) {
    static uint32_t match_array[] = {
        '/','.',':',';',',','[',']','<','>','!','%','^','&','*','(',')','{','}','-','=','+','|',
        '~','"','\''
    };
    bool result = AOC_Rune_match(rune, match_array, AOC_ARRLEN(match_array));
    return result;
}

typedef struct {
    const uint8_t *data;
    int64_t len;
} AOC_String;

#define AOC_STRLIT(s) ((AOC_String){s,sizeof(s)-1})

typedef struct {
    const uint8_t *data;
    int64_t data_len;
    int64_t cursor;
} AOC_State;



bool
AOC_Extract_rune(AOC_State *state, uint32_t *result_rune, int64_t *result_byte_size) {

    int64_t        data_len = state->data_len;
    int64_t        cursor   = state->cursor;
    const uint8_t *data     = state->data;

    uint32_t rune     = 0;
    int64_t byte_size = 0;
    bool success      = false;

    if (cursor < data_len) {
        byte_size = 1;
        uint32_t c0 = data[cursor];
        if ((0x80&c0) == 0) { // 1 byte
            rune    = c0;
            success = true;
        }
        else if ((0xE0&c0)==0xC0) {     // 2 bytes
            if (cursor+2 <= data_len) {
                byte_size   = 2;
                uint32_t c1 = data[cursor+1];
                if ((c1&0xC)==0x80) {
                    rune    = ((c0&0x3F)<<6)|(c1&0x3F);
                    success = true;
                }
            }
        }
        else if ((0xF0&c0)==0xE0) {     // 3 bytes
            if (cursor+3 <= data_len) {
                byte_size   = 3;
                uint32_t c1 = data[cursor+1];
                uint32_t c2 = data[cursor+2];
                if ((c1&0xC)==0x80 && (c2&0xC)==0x80) {
                    rune    = ((c0&0x0F)<<12)|((c1&0x3F)<<6)|((c2&0x3F));
                    success = true;
                }
            }
        }
        else if ((0xF8&c0)==0xF0) {    // 4 bytes
            if (cursor+4 <= data_len) {
                byte_size = 4;
                uint32_t c1 = data[cursor+1];
                uint32_t c2 = data[cursor+2];
                uint32_t c3 = data[cursor+3];
                if ((c1&0xC)==0x80 && (c2&0xC)==0x80 && (c3&0xC)==0x80) {
                    rune    = ((c0&0x07)<<18)|((c1&0x3F)<<12)|((c2&0x3F)<<6)|((c3&0x3F));
                    success = true;
                }
            }
        }
    }
    if (result_rune)      *result_rune = rune;
    if (result_byte_size) *result_byte_size = byte_size;
    return success;
}


void
AOC_Eat_spaces(AOC_State *state) {
    for (;;) {
        uint32_t rune;
        int64_t  rune_byte_size;
        if (!AOC_Extract_rune(state, &rune, &rune_byte_size)) return;
        if (!AOC_Rune_is_whitespace(rune)) return;
        if (rune == 0) return;
        state->cursor += rune_byte_size;
    }
}


bool
AOC_Next_token(AOC_State *state, AOC_String *result) {
    AOC_Eat_spaces(state);
    if (state->cursor >= state->data_len) return false;
    result->data = &(state->data[state->cursor]);
    result->len = 1;
    uint32_t rune;
    int64_t  rune_byte_size;
    if (!AOC_Extract_rune(state, &rune, &rune_byte_size)) return false;
    state->cursor += rune_byte_size;
    if (AOC_Rune_is_symbol(rune)) {
        
    }
    else {
        for (;;) {
            if (!AOC_Extract_rune(state, &rune, &rune_byte_size)) break;
            if (AOC_Rune_is_symbol(rune)||AOC_Rune_is_whitespace(rune)) break;
            state->cursor += rune_byte_size;
            result->len+=1;
        }
    }
    return true;
}



int
main(void) {
    uint8_t data[] = 
    "int add(int a, int b) {\n"
    "   int result = a+b;\n"
    "   return result;\n"
    "}\n";
    AOC_State s;
    s.data = data;
    s.cursor = 0;
    s.data_len = sizeof(data)-1;
    AOC_String token;
    for (;AOC_Next_token(&s, &token);) {
        printf("Token: %.*s\n", (int)token.len, token.data);
    }
    return 0;
}


