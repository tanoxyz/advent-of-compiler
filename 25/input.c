
int
add(int a, int b) {
    int result = a+b;
    return result;
}


int
mul(int a, int b) {
    int result = a*b;
    return result;
}


int
foo() {
    int a = 10;
    return a+3*3; // 19
}

int
bar() {
    return 0xFFFFFFFF; // -1
}

