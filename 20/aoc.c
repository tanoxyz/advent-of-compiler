
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <math.h>


#define AOC_ARENA_CAPACITY (4*1024*1024) // 4mb
typedef struct AOC_Arena AOC_Arena;
struct AOC_Arena {
    AOC_Arena *prev;
    AOC_Arena *next;
    AOC_Arena *current;
    int64_t cursor;
    uint8_t data[AOC_ARENA_CAPACITY];
};

AOC_Arena *AOC_free_arenas = NULL;

AOC_Arena *
AOC_Arena_Allocate(void) {
    AOC_Arena *result = NULL;
    if (AOC_free_arenas) {
        result = AOC_free_arenas;
        AOC_free_arenas = AOC_free_arenas->prev;
    }
    else {
        uint8_t *allocated_data = sbrk(sizeof(AOC_Arena)+(sizeof(void*)-1));
        uintptr_t allocated_data_int = (uintptr_t)allocated_data;
        allocated_data_int = (allocated_data_int+(sizeof(void*)-1))&(~(sizeof(void*)-1)); // Align
        result = (AOC_Arena *)allocated_data_int;
    }
    result->prev = NULL;
    result->next = NULL;
    result->current = result;
    result->cursor = 0;
    return result;
}

void
AOC_Arena_Release(AOC_Arena *arena) {
    arena->prev = AOC_free_arenas;
    AOC_free_arenas = arena->current;
}

void *
AOC_Arena_Push_aligned(AOC_Arena *arena, int64_t total_bytes, int64_t alignement) {
    AOC_Arena *arena_curr   = arena->current;
    uintptr_t start_address = (uintptr_t)(arena_curr->data+arena_curr->cursor);
    uintptr_t end_address   = (uintptr_t)(arena_curr->data+AOC_ARENA_CAPACITY);
    uintptr_t start_address_aligned = (start_address+(alignement-1))-((start_address+(alignement-1))%alignement);
    arena_curr->cursor     += (start_address_aligned+total_bytes)-start_address;
    void *result = (void *)start_address_aligned;

    // Not enough memory in the current arena, allocate a new arena
    if (start_address_aligned+total_bytes > end_address) {
        AOC_Arena *arena_new = AOC_Arena_Allocate();
        start_address = (uintptr_t)(arena_new->data);
        end_address   = (uintptr_t)(arena_new->data+AOC_ARENA_CAPACITY);
        start_address_aligned = (start_address+(alignement-1))-((start_address+(alignement-1))%alignement);

        if (start_address_aligned+total_bytes > end_address) {
            result = NULL; // Didn't fit anyway
        }
        else {
            arena_new->cursor    = (start_address_aligned+total_bytes)-start_address;
            result = (void *)start_address_aligned;
        }
        
        arena_curr->next = arena_new;
        arena_new->prev  = arena_curr;
        arena->current   = arena_new;
    }

    return result;
}



#define AOC_ARENA_NEW(arena, T) ((T*)AOC_Arena_Push_aligned(arena, sizeof(T), sizeof(void*)))
#define AOC_ARENA_NEW_ZERO(arena, T) ((T*)memset(AOC_Arena_Push_aligned(arena, sizeof(T), sizeof(void*)),0,sizeof(T)))

#define AOC_ARENA_NEW_ARRAY(arena, elements, T) ((T*)AOC_Arena_Push_aligned(arena, sizeof(T)*elements, sizeof(void*)))


#define AOC_ARRLEN(a) (sizeof(a)/sizeof(a[0]))


typedef struct {
    AOC_Arena *arena;
    int64_t cursor;
} AOC_ArenaCheckpoint;


AOC_ArenaCheckpoint
AOC_Arena_Get_checkpoint(AOC_Arena *arena) {
    AOC_Arena *arena_curr = arena->current;
    AOC_ArenaCheckpoint result;
    result.arena  = arena_curr;
    result.cursor = arena_curr->cursor;
    return result;
}

void
AOC_Arena_Restore_checkpoint(AOC_Arena *arena, AOC_ArenaCheckpoint checkpoint) {
    AOC_Arena *arena_curr = arena->current;
    if (arena_curr != checkpoint.arena) {
        AOC_Arena *arena_prev = arena_curr->prev;
        AOC_Arena_Release(arena_curr);
        arena_curr = arena_prev;
    }
    arena_curr->cursor = checkpoint.cursor;
    arena->current = arena_curr;
}


typedef struct {
    AOC_Arena *arena;
    AOC_ArenaCheckpoint checkpoint;
} AOC_ArenaTemp;

#define AOC_SCRATCH_POOL_COUNT 4

AOC_Arena *AOC_scratch_pool[AOC_SCRATCH_POOL_COUNT] = {0};

AOC_ArenaTemp
AOC_Arena_Get_scratch(AOC_Arena *conflicts[], int64_t conflicts_count) {
    AOC_ArenaTemp result = {0};
    int64_t selected_scratch = -1;
    for (int64_t scratch_i=0; scratch_i < AOC_SCRATCH_POOL_COUNT; scratch_i+=1) {
        if (AOC_scratch_pool[scratch_i] == NULL) {
            AOC_scratch_pool[scratch_i] = AOC_Arena_Allocate();
            selected_scratch = scratch_i;
            break;
        }

        bool we_have_conflict = false;
        for (int64_t conflict_i=0; conflict_i < conflicts_count; conflict_i+=1) {
            if (AOC_scratch_pool[scratch_i] == conflicts[conflict_i]) {
                we_have_conflict = true;
                break;
            }
        }

        if (!we_have_conflict) {
            selected_scratch = scratch_i;
            break;
        }
    }

    if (selected_scratch != -1) {
        result.arena = AOC_scratch_pool[selected_scratch];
        result.checkpoint = AOC_Arena_Get_checkpoint(result.arena);
    }

    return result;
}

void
AOC_Arena_Release_scratch(AOC_ArenaTemp scratch) {
    // TODO: Release the arena if is empty
    AOC_Arena_Restore_checkpoint(scratch.arena, scratch.checkpoint);
}

#define AOC_WITH_SCRATCH(scratch, conflicts, conflicts_count) \
    int _i_=(scratch=AOC_Arena_Get_scratch(conflicts,conflicts_count),0); !_i_; _i_+=1,AOC_Arena_Release_scratch(scratch)


int64_t
AOC_Rune_match(uint32_t rune, uint32_t match_array[], int64_t match_array_count) {
    for (int64_t i=0; i < match_array_count; i+=1) {
        if (rune == match_array[i]) return i;
    }
    return -1;
}

bool
AOC_Rune_is_whitespace(uint32_t rune) {
    static uint32_t match_array[] = {' ', '\n', '\r', '\t'};
    bool result = (AOC_Rune_match(rune, match_array, AOC_ARRLEN(match_array)) >= 0);
    return result;
}

bool
AOC_Rune_is_alpha(uint32_t rune) {
    static uint32_t match_array[] = {
        'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v',
        'w','x','y','z', 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R',
        'S','T','U','V','W','X','Y','Z'
    };
    bool result = (AOC_Rune_match(rune, match_array, AOC_ARRLEN(match_array)) >= 0);
    return result;
}

bool
AOC_Rune_is_numeric(uint32_t rune) {
    static uint32_t match_array[] = {'1','2','3','4','5','6','7','8','9','0'};
    bool result = (AOC_Rune_match(rune, match_array, AOC_ARRLEN(match_array)) >= 0);
    return result;
}

bool
AOC_Rune_is_symbol(uint32_t rune) {
    static uint32_t match_array[] = {
        '/','.',':',';',',','[',']','<','>','!','%','^','&','*','(',')','{','}','-','=','+','|',
        '~','"','\''
    };
    bool result = (AOC_Rune_match(rune, match_array, AOC_ARRLEN(match_array)) >= 0);
    return result;
}


typedef struct {
    const uint8_t *data;
    int64_t len;
} AOC_String;

#define AOC_STRLIT(s) ((AOC_String){(const uint8_t *)s,sizeof(s)-1})
#define AOC_STRVARG(s) ((int)(s).len), ((s).data)

bool
AOC_String_Match(AOC_String s1, AOC_String s2) {
    if (s1.len != s2.len) return false;
    for (int64_t cursor = 0; cursor < s1.len; cursor+=1) {
        if (s1.data[cursor] != s2.data[cursor]) return false;
    }
    return true;
}


enum {
    AOC_NUMBER_KIND_NULL,
    AOC_NUMBER_KIND_INT,
    AOC_NUMBER_KIND_UNSIGNED_INT,
    AOC_NUMBER_KIND_LONG_INT,
    AOC_NUMBER_KIND_UNSIGNED_LONG_INT,
    AOC_NUMBER_KIND_LONG_LONG_INT,
    AOC_NUMBER_KIND_UNSIGNED_LONG_LONG_INT,
    AOC_NUMBER_KIND_FLOAT,
    AOC_NUMBER_KIND_DOUBLE,
};

typedef struct {
    int64_t kind;
    union {
        int64_t  value_int;
        uint64_t value_uint;
        float    value_float;
        double   value_double;
    };
} AOC_Number;


int64_t
AOC_String_decode_number(AOC_String s, uint64_t base, uint64_t *value_, uint64_t *base_exp_digits_) {
    uint64_t value           = 0;
    uint64_t base_exp_digits = 1;
    int64_t cursor = 0;
    for (;cursor < s.len; cursor+=1) {
        uint32_t rune = s.data[cursor];
        int64_t digit = 16;
        if (rune >= '0' && rune <= '9') {
            digit = rune-'0';
        }
        else if (rune >= 'A' && rune <= 'F') {
            digit = rune-'A'+10;
        }
        else if (rune >= 'a' && rune <= 'f') {
            digit = rune-'a'+10;
        }
        if (digit >= base) break;
        value           = value*base+digit;
        base_exp_digits = base_exp_digits*base;
    }

    if (value_) *value_ = value;
    if (base_exp_digits_) *base_exp_digits_ = base_exp_digits;

    return cursor;
}


AOC_Number
AOC_String_to_number(AOC_String s) {
    bool malformed = false;
    int64_t cursor = 0;
    uint64_t int_value      = 0;
    uint64_t frac_value     = 0;
    uint64_t frac_value_div = 1;
    uint64_t  exponent      = 0;
    int64_t  exponent_sign = 1;
    bool is_float = false;
    uint64_t base = 10;

    if (!malformed && cursor < s.len && s.data[cursor] == '0') {
        if (cursor+1 < s.len) {
            uint32_t r = s.data[cursor+1];
            if (r == 'x' || r == 'X') {
                base = 16;
                cursor += 2;
            }
            else if (r == 'b' || r == 'B') {
                base = 2;
                cursor += 2;
            }
            else if (AOC_Rune_is_numeric(r)) {
                base = 8;
                cursor += 1;
            }
        }
    }

    if (!malformed) {
        AOC_String int_string;
        int_string.data = &s.data[cursor];
        int_string.len  = s.len - cursor;
        cursor +=  AOC_String_decode_number(int_string, base, &int_value, NULL);
    }

    if (!malformed && cursor < s.len) {
        uint32_t rune = s.data[cursor];
        if (rune == '.') {
            if (base == 10) {
                is_float = true;
                cursor += 1;
                AOC_String frac_string;
                frac_string.data = &s.data[cursor];
                frac_string.len  = s.len - cursor;
                cursor += AOC_String_decode_number(frac_string, 10, &frac_value, &frac_value_div);
            }
            else {
                malformed = true;
            }
        }
    }

    if (!malformed && cursor < s.len) {
        uint32_t rune = s.data[cursor];
        if (rune == 'e' || rune == 'E') {
            if (base == 10) {
                is_float = true;
                exponent_sign = 1;
                cursor += 1;

                // Decode the sign
                if (cursor+1 < s.len) {
                    if (s.data[cursor] == '-') {
                        exponent_sign = -1;
                        cursor += 1;
                    }
                    else if (s.data[cursor] == '+') {
                        cursor += 1;
                    }
                }

                AOC_String exp_string;
                exp_string.data = &s.data[cursor];
                exp_string.len  = s.len - cursor;
                cursor += AOC_String_decode_number(exp_string, 10, &exponent, NULL);
            }
            else {
                malformed = true;
            }
        }
    }

    if (cursor < s.len) {
        malformed = true;
    }
    
    AOC_Number result = {0};
    if (!malformed) {
        if (is_float) {
            double int_value_double = (double)int_value;
            double frac_value_double = ((double)frac_value)/((double)frac_value_div);
            double exponent_raise = pow(10.0, ((double)exponent)*((double)exponent_sign));
            result.value_double = (int_value_double+frac_value_double)*exponent_raise;
            result.kind = AOC_NUMBER_KIND_DOUBLE;
        }
        else {
            result.value_uint = int_value;
            result.kind       = AOC_NUMBER_KIND_UNSIGNED_LONG_LONG_INT;
        }
    }

    return result;
}




bool
AOC_Extract_rune(AOC_String s, int64_t cursor, uint32_t *result_rune, int64_t *result_byte_size) {

    uint32_t rune     = 0;
    int64_t byte_size = 0;
    bool success      = false;

    if (cursor < s.len) {
        byte_size = 1;
        uint32_t c0 = s.data[cursor];
        if ((0x80&c0) == 0) { // 1 byte
            rune    = c0;
            success = true;
        }
        else if ((0xE0&c0)==0xC0) {     // 2 bytes
            if (cursor+2 <= s.len) {
                byte_size   = 2;
                uint32_t c1 = s.data[cursor+1];
                if ((c1&0xC0)==0x80) {
                    rune    = ((c0&0x3F)<<6)|(c1&0x3F);
                    success = true;
                }
            }
        }
        else if ((0xF0&c0)==0xE0) {     // 3 bytes
            if (cursor+3 <= s.len) {
                byte_size   = 3;
                uint32_t c1 = s.data[cursor+1];
                uint32_t c2 = s.data[cursor+2];
                if ((c1&0xC0)==0x80 && (c2&0xC0)==0x80) {
                    rune    = ((c0&0x0F)<<12)|((c1&0x3F)<<6)|((c2&0x3F));
                    success = true;
                }
            }
        }
        else if ((0xF8&c0)==0xF0) {    // 4 bytes
            if (cursor+4 <= s.len) {
                byte_size = 4;
                uint32_t c1 = s.data[cursor+1];
                uint32_t c2 = s.data[cursor+2];
                uint32_t c3 = s.data[cursor+3];
                if ((c1&0xC0)==0x80 && (c2&0xC0)==0x80 && (c3&0xC0)==0x80) {
                    rune    = ((c0&0x07)<<18)|((c1&0x3F)<<12)|((c2&0x3F)<<6)|((c3&0x3F));
                    success = true;
                }
            }
        }
    }
    if (result_rune)      *result_rune = rune;
    if (result_byte_size) *result_byte_size = byte_size;
    return success;
}


int64_t
AOC_Eat_spaces(AOC_String s, int64_t cursor) {
    for (;;) {
        uint32_t rune;
        int64_t  rune_byte_size;
        if (!AOC_Extract_rune(s, cursor, &rune, &rune_byte_size)) break;
        if (!AOC_Rune_is_whitespace(rune)) break;
        if (rune == 0) break;
        cursor += rune_byte_size;
    }
    return cursor;
}

enum {
    AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION = (1<<0),
    AOC_TOKEN_FLAG_TYPE                       = (1<<1),
    AOC_TOKEN_FLAG_KEYWORD                    = (1<<2),
    AOC_TOKEN_FLAG_EXPRESSION_SYMBOL          = (1<<3),
} AOC_TOKEN_FLAG;

#define AOC_TOKEN_KIND_XMACRO(x) \
    x(AOC_TOKEN_KIND_NULL,             0) \
    x(AOC_TOKEN_KIND_SEMICOLON,        0) \
    x(AOC_TOKEN_KIND_OPEN_PAREN,       0) \
    x(AOC_TOKEN_KIND_CLOSE_PAREN,      0) \
    x(AOC_TOKEN_KIND_OPEN_CURLYBRACE,  0) \
    x(AOC_TOKEN_KIND_CLOSE_CURLYBRACE, 0) \
    x(AOC_TOKEN_KIND_OPEN_BRACKET,     0) \
    x(AOC_TOKEN_KIND_CLOSE_BRACKET,    0) \
    x(AOC_TOKEN_KIND_COMMA,            0) \
    x(AOC_TOKEN_KIND_PLUS,             AOC_TOKEN_FLAG_EXPRESSION_SYMBOL) \
    x(AOC_TOKEN_KIND_HYPEN,            AOC_TOKEN_FLAG_EXPRESSION_SYMBOL) \
    x(AOC_TOKEN_KIND_ASTERISK,         AOC_TOKEN_FLAG_EXPRESSION_SYMBOL) \
    x(AOC_TOKEN_KIND_EQUAL,            AOC_TOKEN_FLAG_EXPRESSION_SYMBOL) \
    x(AOC_TOKEN_KIND_NUMBER,           AOC_TOKEN_FLAG_EXPRESSION_SYMBOL) \
    x(AOC_TOKEN_KIND_IDENTIFIER,       AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION)

#define AOC_TOKEN_KIND_KEYWORD_XMACRO(x) \
    x(AOC_TOKEN_KIND_KEYWORD_AUTO,     AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "auto"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_DOUBLE,   AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "double"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_INT,      AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "int"     ) \
    x(AOC_TOKEN_KIND_KEYWORD_STRUCT,   AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "struct"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_BREAK,    AOC_TOKEN_FLAG_KEYWORD|0,                                                             "break"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_ELSE,     AOC_TOKEN_FLAG_KEYWORD|0,                                                             "else"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_LONG,     AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "long"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_SWITH,    AOC_TOKEN_FLAG_KEYWORD|0,                                                             "swith"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_CASE,     AOC_TOKEN_FLAG_KEYWORD|0,                                                             "case"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_ENUM,     AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "enum"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_REGISTER, AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "register") \
    x(AOC_TOKEN_KIND_KEYWORD_TYPEDEF,  AOC_TOKEN_FLAG_KEYWORD|0,                                                             "typedef" ) \
    x(AOC_TOKEN_KIND_KEYWORD_CHAR,     AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "char"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_EXTERN,   AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "extern"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_RETURN,   AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "return"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_UNION,    AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "union"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_CONST,    AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "const"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_FLOAT,    AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "float"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_SHORT,    AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "short"   ) \
    x(AOC_TOKEN_KIND_KEYWORD_UNSIGNED, AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "unsigned") \
    x(AOC_TOKEN_KIND_KEYWORD_CONTINUE, AOC_TOKEN_FLAG_KEYWORD|0,                                                             "continue") \
    x(AOC_TOKEN_KIND_KEYWORD_FOR,      AOC_TOKEN_FLAG_KEYWORD|0,                                                             "for"     ) \
    x(AOC_TOKEN_KIND_KEYWORD_SIGNED,   AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "signed"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_VOID,     AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_TYPE|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION, "void"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_DEFAULT,  AOC_TOKEN_FLAG_KEYWORD|0,                                                             "default" ) \
    x(AOC_TOKEN_KIND_KEYWORD_GOTO,     AOC_TOKEN_FLAG_KEYWORD|0,                                                             "goto"    ) \
    x(AOC_TOKEN_KIND_KEYWORD_SIZEOF,   AOC_TOKEN_FLAG_KEYWORD|0,                                                             "sizeof"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_VOLATILE, AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "volatile") \
    x(AOC_TOKEN_KIND_KEYWORD_DO,       AOC_TOKEN_FLAG_KEYWORD|0,                                                             "do"      ) \
    x(AOC_TOKEN_KIND_KEYWORD_IF,       AOC_TOKEN_FLAG_KEYWORD|0,                                                             "if"      ) \
    x(AOC_TOKEN_KIND_KEYWORD_STATIC,   AOC_TOKEN_FLAG_KEYWORD|AOC_TOKEN_FLAG_VALID_START_OF_DECLARATION,                     "static"  ) \
    x(AOC_TOKEN_KIND_KEYWORD_WHILE,    AOC_TOKEN_FLAG_KEYWORD|0,                                                             "while"   )


typedef enum {
    #define x(token_kind, flags) token_kind,
    AOC_TOKEN_KIND_XMACRO(x)
    #undef x
    #define x(token_kind, flags, keyword_str) token_kind,
    AOC_TOKEN_KIND_KEYWORD_XMACRO(x)
    #undef x
} AOC_TokenKind;

AOC_String AOC_TokenKind_to_string[] = {
    #define x(token_kind, flags) [token_kind] = AOC_STRLIT(# token_kind),
    AOC_TOKEN_KIND_XMACRO(x)
    #undef x
    #define x(token_kind, flags, keyword_str) [token_kind] = AOC_STRLIT(# token_kind),
    AOC_TOKEN_KIND_KEYWORD_XMACRO(x)
    #undef x
};

uint64_t AOC_TokenKind_to_flags[] = {
    #define x(token_kind, flags) [token_kind] = flags,
    AOC_TOKEN_KIND_XMACRO(x)
    #undef x
    #define x(token_kind, flags, keyword_str) [token_kind] = flags,
    AOC_TOKEN_KIND_KEYWORD_XMACRO(x)
    #undef x
};

typedef struct {
    int64_t    kind;
    AOC_String string;
} AOC_TokenKeywordStringPair;

AOC_TokenKeywordStringPair AOC_token_keyword_string_table[] = {
    #define x(token_kind, flags, keyword_str) {token_kind, AOC_STRLIT(keyword_str)},
    AOC_TOKEN_KIND_KEYWORD_XMACRO(x)
    #undef x
};

bool
AOC_String_Is_keyword(AOC_String s, int64_t *token_keyword_kind) {
    for (int64_t i = 0; i < AOC_ARRLEN(AOC_token_keyword_string_table); i+=1) {
        if (AOC_String_Match(s, AOC_token_keyword_string_table[i].string)) {
            if (token_keyword_kind) *token_keyword_kind = AOC_token_keyword_string_table[i].kind;
            return true;
        }
    }
    return false;
}


typedef struct {
    AOC_String filename;
    int64_t line;
    int64_t column;
} AOC_FileCoordinates;

#define AOC_COORDFMT  "%.*s:%lu:%lu"
#define AOC_COORDVARG(coords) AOC_STRVARG((coords).filename), (coords).line, (coords).column

typedef struct AOC_Token AOC_Token;
struct AOC_Token {
    AOC_FileCoordinates coordinates;
    AOC_Token *next;
    AOC_String string;
    AOC_TokenKind kind;
};


typedef struct {
    AOC_String s;
    int64_t cursor;
    AOC_FileCoordinates coordinates;
} AOC_Tokenizer;


void
AOC_Print_token(AOC_Token token) {
    AOC_String kind_str = AOC_TokenKind_to_string[token.kind];
    printf(AOC_COORDFMT": Kind: %d \"%.*s\" String: \"%.*s\"", AOC_COORDVARG(token.coordinates), token.kind, AOC_STRVARG(kind_str), AOC_STRVARG(token.string));
}

void
AOC_Print_error(AOC_FileCoordinates coords, const char *fmt, ...) {
    printf(AOC_COORDFMT": Error: ", AOC_COORDVARG(coords));
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
    puts("");
}

bool
AOC_Next_token(AOC_Tokenizer *tokenizer, AOC_Token *token_result) {

    // Eat spaces
    for (;;) {
        uint32_t rune;
        int64_t  rune_byte_size;
        if (!AOC_Extract_rune(tokenizer->s, tokenizer->cursor, &rune, &rune_byte_size)) break;
        if (rune == 0) break;
        if (rune == ' ' || rune == '\t') {
            tokenizer->coordinates.column += 1;
        }
        else if (rune == '\n') {
            tokenizer->coordinates.line += 1;
            tokenizer->coordinates.column = 1;
        }
        else if (rune != '\r') {
            break;
        }
        tokenizer->cursor += rune_byte_size;
    }

    bool       error        = false;
    int64_t    token_kind   = AOC_TOKEN_KIND_NULL;
    AOC_String token_string = AOC_STRLIT("");
    AOC_FileCoordinates token_coordinates = {0};

    if (tokenizer->cursor >= tokenizer->s.len) error = true;
    
    // Extract the first rune
    uint32_t rune;
    int64_t  rune_byte_size;
    if (!error) {
        if (!AOC_Extract_rune(tokenizer->s, tokenizer->cursor, &rune, &rune_byte_size)) {
            error = true;
        }
    }
    if (!error) {
        token_string.data = &(tokenizer->s.data[tokenizer->cursor]);
        token_string.len  = rune_byte_size;
        token_coordinates = tokenizer->coordinates;
        tokenizer->cursor += rune_byte_size;
        tokenizer->coordinates.column += 1;
        if      (rune == ';') token_kind = AOC_TOKEN_KIND_SEMICOLON;
        else if (rune == '(') token_kind = AOC_TOKEN_KIND_OPEN_PAREN;
        else if (rune == ')') token_kind = AOC_TOKEN_KIND_CLOSE_PAREN;
        else if (rune == '{') token_kind = AOC_TOKEN_KIND_OPEN_CURLYBRACE;
        else if (rune == '}') token_kind = AOC_TOKEN_KIND_CLOSE_CURLYBRACE;
        else if (rune == '[') token_kind = AOC_TOKEN_KIND_OPEN_BRACKET;
        else if (rune == ']') token_kind = AOC_TOKEN_KIND_CLOSE_BRACKET;
        else if (rune == ',') token_kind = AOC_TOKEN_KIND_COMMA;
        else if (rune == '+') token_kind = AOC_TOKEN_KIND_PLUS;
        else if (rune == '-') token_kind = AOC_TOKEN_KIND_HYPEN;
        else if (rune == '=') token_kind = AOC_TOKEN_KIND_EQUAL;
        else if (rune == '*') token_kind = AOC_TOKEN_KIND_ASTERISK;
        else if (AOC_Rune_is_numeric(rune)) {
            token_kind = AOC_TOKEN_KIND_NUMBER;
            for (;;) {
                if (!AOC_Extract_rune(tokenizer->s, tokenizer->cursor, &rune, &rune_byte_size)) break;
                if (AOC_Rune_is_symbol(rune)||AOC_Rune_is_whitespace(rune)) break;
                tokenizer->cursor += rune_byte_size;
                tokenizer->coordinates.column += 1;
                token_string.len += rune_byte_size;
            }
        }
        else {
            token_kind = AOC_TOKEN_KIND_IDENTIFIER;
            for (;;) {
                if (!AOC_Extract_rune(tokenizer->s, tokenizer->cursor, &rune, &rune_byte_size)) break;
                if (AOC_Rune_is_symbol(rune)||AOC_Rune_is_whitespace(rune)) break;
                tokenizer->cursor += rune_byte_size;
                tokenizer->coordinates.column += 1;
                token_string.len  += rune_byte_size;
            }
            int64_t token_keyword_kind;
            if (AOC_String_Is_keyword(token_string, &token_keyword_kind)) {
                token_kind = token_keyword_kind;
            }
            else {
                token_kind = AOC_TOKEN_KIND_IDENTIFIER;
            }
        }
    }
    if (token_result) {
        token_result->kind   = token_kind;
        token_result->string = token_string;
        token_result->coordinates = token_coordinates;
    }
    return !error;
}


AOC_Token *
AOC_Tokenize(AOC_Arena *arena, AOC_String s) {
    AOC_Tokenizer tokenizer = {.s = s, .cursor = 0, .coordinates = {.filename = AOC_STRLIT(""), .line = 1, .column = 1}};
    AOC_Token *result     = NULL;
    AOC_Token *last_token = NULL;
    AOC_Token token;
    for (;;) {
        AOC_Next_token(&tokenizer, &token);
        AOC_Token *allocated_token = AOC_ARENA_NEW(arena, AOC_Token);
        *allocated_token = token;
        allocated_token->next = NULL;
        if (!result)    result           = allocated_token;
        if (last_token) last_token->next = allocated_token;
        last_token = allocated_token;
        if (token.kind == AOC_TOKEN_KIND_NULL) break;
    }
    return result;
}


#define AOC_AST_NODE_KIND_XMACRO(x)  \
    x(AOC_AST_NODE_KIND_NULL)        \
    x(AOC_AST_NODE_KIND_FUNCTION)    \
    x(AOC_AST_NODE_KIND_DECLARATION) \
    x(AOC_AST_NODE_KIND_EXPRESION)   \
    x(AOC_AST_NODE_KIND_TYPE)        \
    x(AOC_AST_NODE_KIND_RETURN)


enum {
    #define x(kind) kind,
    AOC_AST_NODE_KIND_XMACRO(x)
    #undef x
};

#define AOC_AST_TYPE_KIND_XMACRO(x)  \
    x(AOC_AST_TYPE_KIND_NULL,                   "null")                   \
    x(AOC_AST_TYPE_KIND_VOID,                   "void")                   \
    x(AOC_AST_TYPE_KIND_CHAR,                   "char")                   \
    x(AOC_AST_TYPE_KIND_UNSIGNED_CHAR,          "unsigned char")          \
    x(AOC_AST_TYPE_KIND_INT,                    "int")                    \
    x(AOC_AST_TYPE_KIND_SHORT_INT,              "short int")              \
    x(AOC_AST_TYPE_KIND_LONG_INT,               "long int")               \
    x(AOC_AST_TYPE_KIND_LONG_LONG_INT,          "long long int")          \
    x(AOC_AST_TYPE_KIND_UNSIGNED_INT,           "unsigned int")           \
    x(AOC_AST_TYPE_KIND_UNSIGNED_SHORT_INT,     "unsigned short int")     \
    x(AOC_AST_TYPE_KIND_UNSIGNED_LONG_INT,      "unsigned long int")      \
    x(AOC_AST_TYPE_KIND_UNSIGNED_LONG_LONG_INT, "unsigned long long int") \
    x(AOC_AST_TYPE_KIND_FLOAT,                  "float")                  \
    x(AOC_AST_TYPE_KIND_DOUBLE,                 "float")                  \
    x(AOC_AST_TYPE_KIND_STRUCT,                 "struct")                 \
    x(AOC_AST_TYPE_KIND_TYPEDEF,                "typedef")                \


enum {
    #define x(kind, string) kind,
    AOC_AST_TYPE_KIND_XMACRO(x)
    #undef x
};


AOC_String AOC_AstTypeKind_to_string[] = {
    #define x(kind, string) [kind] = AOC_STRLIT(string),
    AOC_AST_TYPE_KIND_XMACRO(x)
    #undef x
};

enum {
    AOC_AST_STORAGE_KIND_NULL = 0,
    AOC_AST_STORAGE_KIND_AUTO,
    AOC_AST_STORAGE_KIND_STATIC
};


typedef struct AOC_Expression AOC_Expression;

typedef struct AOC_AstNode AOC_AstNode;

typedef struct AOC_Scope    AOC_Scope;
typedef struct AOC_Function AOC_Function;
typedef struct AOC_Declaration AOC_Declaration;


typedef struct {
    bool is_const;
    bool is_volatile;
    int64_t storage_kind;
    int64_t kind;
} AOC_Type;


struct AOC_Function {
    AOC_Function *next;

    AOC_String name;
    AOC_Type return_type;

    AOC_Declaration *parameters;
    int64_t parameters_count;

    AOC_AstNode *body;
};

struct AOC_Scope {
    AOC_Function *functions;
    AOC_Function *functions_last;
    int64_t       functions_count;
    AOC_AstNode *other_nodes;
};

struct AOC_Declaration {
    AOC_Declaration *next;
    AOC_String name;
    AOC_Type type;
    AOC_Expression *assignment;
};


struct AOC_AstNode {
    AOC_AstNode *next;
    int64_t kind;
    union {
        AOC_Declaration *declaration;
        AOC_Expression  *expression;
    };
};


struct AOC_Expression {
    int64_t kind;
    union {
        struct {
            AOC_String string;
        } identifier;
        struct {
            AOC_Expression *left;
            AOC_Expression *right;
        } binary;
        struct {
            AOC_Expression *child;
        } unary;
    };
};

enum {
    AOC_EXPRESSION_FLAG_BINARY     = (0x1<<0),
    AOC_EXPRESSION_FLAG_UNARY_PRE  = (0x1<<1),
    AOC_EXPRESSION_FLAG_UNARY_POST = (0x1<<1),
};

enum {
    AOC_EXPRESSION_KIND_NULL,

    AOC_EXPRESSION_KIND_LEAF_BEGIN,
        AOC_EXPRESSION_KIND_NUMERIC_VALUE,
        AOC_EXPRESSION_KIND_IDENTIFIER_VALUE,
    AOC_EXPRESSION_KIND_LEAF_END,

    AOC_EXPRESSION_KIND_BINARY_BEGIN,
        AOC_EXPRESSION_KIND_ADD,
        AOC_EXPRESSION_KIND_SUBSTRACT,
        AOC_EXPRESSION_KIND_MULTIPLY,
        AOC_EXPRESSION_KIND_DIVIDE,
        AOC_EXPRESSION_KIND_AND,
        AOC_EXPRESSION_KIND_OR,
        AOC_EXPRESSION_KIND_XOR,
        AOC_EXPRESSION_KIND_LOGIC_AND,
        AOC_EXPRESSION_KIND_LOGIC_OR,
        AOC_EXPRESSION_KIND_EQUAL,
        AOC_EXPRESSION_KIND_LESS,
        AOC_EXPRESSION_KIND_LESS_EQUAL,
        AOC_EXPRESSION_KIND_GREATER,
        AOC_EXPRESSION_KIND_GREATER_EQUAL,
        AOC_EXPRESSION_KIND_ACCESS_FIELD,
        AOC_EXPRESSION_KIND_DEREF_ACCESS_FIELD,
        AOC_EXPRESSION_KIND_ASSIGN,
    AOC_EXPRESSION_KIND_BINARY_END,

    AOC_EXPRESSION_KIND_UNARY_BEGIN,
        AOC_EXPRESSION_KIND_UNARY_PRE_BEGIN,
            AOC_EXPRESSION_KIND_PLUS,
            AOC_EXPRESSION_KIND_MINUS,
            AOC_EXPRESSION_KIND_INVERT,
            AOC_EXPRESSION_KIND_PREINCREMENT,
            AOC_EXPRESSION_KIND_PREDECREMENT,
            AOC_EXPRESSION_KIND_DEREFERENCE,
            AOC_EXPRESSION_KIND_REFERENCE,
            AOC_EXPRESSION_KIND_CAST,
        AOC_EXPRESSION_KIND_UNARY_PRE_END,
        AOC_EXPRESSION_KIND_UNARY_POST_BEGIN,
            AOC_EXPRESSION_KIND_POSTINCREMENT,
            AOC_EXPRESSION_KIND_POSTDECREMENT,
            AOC_EXPRESSION_KIND_FUNCTION_CALL,
            AOC_EXPRESSION_KIND_INDEXING,
        AOC_EXPRESSION_KIND_UNARY_POST_END,
    AOC_EXPRESSION_KIND_UNARY_END,
};


bool
AOC_Expression_is_leaf(AOC_Expression *expr) {
    bool result = (expr->kind > AOC_EXPRESSION_KIND_LEAF_BEGIN && expr->kind < AOC_EXPRESSION_KIND_LEAF_END);
    return result;
}

bool
AOC_Expression_is_binary(AOC_Expression *expr) {
    bool result = (expr->kind > AOC_EXPRESSION_KIND_BINARY_BEGIN && expr->kind < AOC_EXPRESSION_KIND_BINARY_END);
    return result;
}

bool
AOC_Expression_is_unary(AOC_Expression *expr) {
    bool result = (expr->kind > AOC_EXPRESSION_KIND_UNARY_BEGIN && expr->kind < AOC_EXPRESSION_KIND_UNARY_END);
    return result;
}

bool
AOC_Expression_is_unary_pre(AOC_Expression *expr) {
    bool result = (expr->kind > AOC_EXPRESSION_KIND_UNARY_PRE_BEGIN && expr->kind < AOC_EXPRESSION_KIND_UNARY_PRE_END);
    return result;
}

bool
AOC_Expression_is_unary_post(AOC_Expression *expr) {
    bool result = (expr->kind > AOC_EXPRESSION_KIND_UNARY_POST_BEGIN && expr->kind < AOC_EXPRESSION_KIND_UNARY_POST_END);
    return result;
}

AOC_Expression *
AOC_Parse_expression(AOC_Arena *arena, AOC_Token *token_node, AOC_Token **last_token) {
    AOC_Expression *expression = AOC_ARENA_NEW_ZERO(arena, AOC_Expression);
    expression->kind = AOC_EXPRESSION_KIND_NULL;

    if (token_node->kind == AOC_TOKEN_KIND_IDENTIFIER || token_node->kind == AOC_TOKEN_KIND_NUMBER) {
        expression->kind   = AOC_EXPRESSION_KIND_IDENTIFIER_VALUE;
        expression->identifier.string = token_node->string;
        AOC_Expression *new_expression = AOC_Parse_expression(arena, token_node->next, &token_node);
        if (AOC_Expression_is_binary(new_expression)) {
            new_expression->binary.left = expression;
            expression = new_expression;
        }
    }
    else if (token_node->kind == AOC_TOKEN_KIND_PLUS) {
        expression->kind   = AOC_EXPRESSION_KIND_ADD;
        expression->binary.right = AOC_Parse_expression(arena, token_node->next, &token_node);
    }
    else if (token_node->kind == AOC_TOKEN_KIND_ASTERISK) {
        expression->kind   = AOC_EXPRESSION_KIND_MULTIPLY;
        expression->binary.right = AOC_Parse_expression(arena, token_node->next, &token_node);
    }

    if (last_token) *last_token = token_node;

    return expression;
}

void
AOC_Print_expression(AOC_Expression *expr_node) {
    if (!expr_node) {
        printf("null");
    }
    else if (expr_node->kind == AOC_EXPRESSION_KIND_NUMERIC_VALUE) {
        printf("%.*s", AOC_STRVARG(expr_node->identifier.string));
    }
    else if (expr_node->kind == AOC_EXPRESSION_KIND_IDENTIFIER_VALUE) {
        printf("%.*s", AOC_STRVARG(expr_node->identifier.string));
    }
    else if (AOC_Expression_is_binary(expr_node)) {
        if (expr_node->kind == AOC_EXPRESSION_KIND_ADD)                     printf("(+ ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_SUBSTRACT)          printf("(- ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_MULTIPLY)           printf("(* ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_DIVIDE)             printf("(/ ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_AND)                printf("(& ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_OR)                 printf("(| ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_XOR)                printf("(^ ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_LOGIC_AND)          printf("(&& ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_LOGIC_OR)           printf("(|| ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_EQUAL)              printf("(== ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_LESS)               printf("(< ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_LESS_EQUAL)         printf("(<= ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_GREATER)            printf("(> ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_GREATER_EQUAL)      printf("(>= ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_ACCESS_FIELD)       printf("(. ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_DEREF_ACCESS_FIELD) printf("(-> ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_ASSIGN)             printf("(= ");
        AOC_Print_expression(expr_node->binary.left);
        printf(" ");
        AOC_Print_expression(expr_node->binary.right);
        printf(")");
    }
    else if (AOC_Expression_is_unary(expr_node)) {
        if (expr_node->kind == AOC_EXPRESSION_KIND_PLUS)                    printf("(+ ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_MINUS)              printf("(- ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_INVERT)             printf("(~ ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_PREINCREMENT)       printf("(pre++ ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_PREDECREMENT)       printf("(pre-- ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_DEREFERENCE)        printf("(* ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_REFERENCE)          printf("(* ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_CAST)               printf("(cast ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_POSTINCREMENT)      printf("(post++ ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_POSTDECREMENT)      printf("(post-- ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_FUNCTION_CALL)      printf("(call ");
        else if (expr_node->kind == AOC_EXPRESSION_KIND_INDEXING)           printf("([] ");
        AOC_Print_expression(expr_node->unary.child);
    }
}







AOC_Type
AOC_Parse_type(AOC_Token *token_node, AOC_Token **last_type_token) {

    int64_t type_kind     = AOC_AST_TYPE_KIND_NULL;
    int64_t storage_kind  = AOC_AST_STORAGE_KIND_NULL;
    int64_t long_count    = 0;
    bool    short_found   = 0;
    bool    typedef_found = 0;
    bool    is_const      = false;
    bool    is_volatile   = false;
    bool    has_errors    = true;


    for (;;token_node = token_node->next) {
        int64_t kind = token_node->kind;
        int64_t new_type    = AOC_AST_TYPE_KIND_NULL;
        int64_t new_storage = AOC_AST_STORAGE_KIND_NULL;
        if (kind == AOC_TOKEN_KIND_KEYWORD_VOID)         new_type = AOC_AST_TYPE_KIND_VOID;
        else if (kind == AOC_TOKEN_KIND_KEYWORD_CHAR)    new_type = AOC_AST_TYPE_KIND_CHAR;
        else if (kind == AOC_TOKEN_KIND_KEYWORD_INT)     new_type = AOC_AST_TYPE_KIND_INT;
        else if (kind == AOC_TOKEN_KIND_KEYWORD_FLOAT)   new_type = AOC_AST_TYPE_KIND_FLOAT;
        else if (kind == AOC_TOKEN_KIND_KEYWORD_DOUBLE)  new_type = AOC_AST_TYPE_KIND_DOUBLE;
        else if (kind == AOC_TOKEN_KIND_KEYWORD_STATIC)  new_storage = AOC_AST_STORAGE_KIND_STATIC;
        else if (kind == AOC_TOKEN_KIND_KEYWORD_AUTO)    new_storage = AOC_AST_STORAGE_KIND_AUTO;
        else if (kind == AOC_TOKEN_KIND_KEYWORD_REGISTER)new_storage = AOC_AST_STORAGE_KIND_AUTO;
        else if (kind == AOC_TOKEN_KIND_KEYWORD_TYPEDEF) {
            if (typedef_found) {
                AOC_Print_error(token_node->coordinates, "typedef keyword found more than once in the same declaration.");
                has_errors = true;
                break;
            }
            typedef_found = true;
        }
        else {
            break;
        }

        if (new_storage != AOC_AST_STORAGE_KIND_NULL && storage_kind != AOC_AST_STORAGE_KIND_NULL) {
            AOC_Print_error(token_node->coordinates, "more than one storage kinds specified.");
            has_errors = true;
            break;
        }
        storage_kind = new_storage;

        if (new_type != AOC_AST_TYPE_KIND_NULL && type_kind != AOC_AST_TYPE_KIND_NULL) {
            AOC_Print_error(token_node->coordinates, "more than one type specified.");
            has_errors = true;
            break;
        }
        type_kind = new_type;

        has_errors = false;
    }

    AOC_Type result = {0};
    if (!has_errors) {
        result.is_const     = is_const;
        result.is_volatile  = is_volatile;
        result.storage_kind = storage_kind;
        result.kind         = type_kind;
    }

    if (last_type_token) *last_type_token = token_node;
    return result;
}





AOC_Declaration *
AOC_Parse_function_parameters(AOC_Arena *arena, int64_t *result_count, AOC_Token *token_node, AOC_Token **last_token) {
    AOC_Declaration *result    = NULL;
    AOC_Declaration *last_node = NULL;
    bool expect_comma = false;
    int64_t params_count = 0;
    for (;;) {
        if (token_node->kind == AOC_TOKEN_KIND_CLOSE_PAREN) {
            token_node = token_node->next;
            break;
        }

        // Comma handling
        if (expect_comma) {
            if (token_node->kind != AOC_TOKEN_KIND_COMMA) {
                AOC_Print_error(token_node->coordinates, "expected \",\" or \")\" but got \"%.*s\"\n", AOC_STRVARG(token_node->string));
                return NULL;
            }
            token_node = token_node->next;
        }
        expect_comma = true;

        uint64_t token_flags = AOC_TokenKind_to_flags[token_node->kind];

        AOC_Type type = AOC_Parse_type(token_node, &token_node);

        AOC_Declaration *declaration = NULL;
        declaration = AOC_ARENA_NEW_ZERO(arena, AOC_Declaration);
        declaration->type = type;

        if (token_node->kind == AOC_TOKEN_KIND_IDENTIFIER) {
            declaration->name = token_node->string;
            token_node = token_node->next;
        }
        else if (type.kind == 0){
            printf("Error, unexpected token %.*s in a parameter list\n", AOC_STRVARG(token_node->string));
            return NULL;
        }

        if (declaration) params_count += 1;
        if (!result) result = declaration;
        if (last_node) last_node->next = declaration;
        last_node = declaration;
    }

    if (last_token) *last_token = token_node;

    if (result_count) *result_count = params_count;

    return result;
}






AOC_AstNode *
AOC_Parse(AOC_Arena *arena, AOC_Scope *scope, bool inside_body, AOC_Token *token_node, AOC_Token **last_token);

AOC_Function *
AOC_Parse_function(AOC_Arena *arena, AOC_Token *token_node, AOC_Token **last_token) {
    int64_t params_count = 0;
    AOC_Declaration *parameters = AOC_Parse_function_parameters(arena, &params_count, token_node->next, &token_node);
    AOC_AstNode *ast_node_body = NULL;
    if (token_node->kind == AOC_TOKEN_KIND_OPEN_CURLYBRACE) {
        token_node = token_node->next;
        ast_node_body = AOC_Parse(arena, NULL, true, token_node, &token_node);
    }
    else if (token_node->kind != AOC_TOKEN_KIND_SEMICOLON) {
        AOC_Print_error(token_node->coordinates, "expected \";\" but got \"%.*s\"", AOC_STRVARG(token_node->string));
    }
    AOC_Function *function = AOC_ARENA_NEW_ZERO(arena, AOC_Function);
    function->parameters       = parameters;
    function->parameters_count = params_count;
    function->body= ast_node_body;
    if (last_token) *last_token = token_node;
    return function;
}


AOC_Declaration *
AOC_Parse_value_decl(AOC_Arena *arena, AOC_Token *token_node, AOC_Token **last_token) {
    AOC_Declaration *declaration = AOC_ARENA_NEW_ZERO(arena, AOC_Declaration);
    if  (token_node->kind == AOC_TOKEN_KIND_EQUAL) {
        declaration->assignment = AOC_Parse_expression(arena, token_node->next, &token_node);
    }
    else if (token_node->kind != AOC_TOKEN_KIND_SEMICOLON) {
        AOC_Print_error(token_node->coordinates, "expected \";\" or \"=\" but got \"%.*s\"", AOC_STRVARG(token_node->string));
    }
    if (last_token) *last_token = token_node;
    return declaration;
}


AOC_AstNode *
AOC_Parse(AOC_Arena *arena, AOC_Scope *scope, bool inside_body, AOC_Token *token_node, AOC_Token **last_token) {
    AOC_AstNode *result    = NULL;
    AOC_AstNode *last_node = NULL;
    for (;;) {
        if (inside_body) {
            if (token_node->kind == AOC_TOKEN_KIND_CLOSE_CURLYBRACE) {
                token_node = token_node->next;
                break;
            }
            else if (token_node->kind == AOC_TOKEN_KIND_NULL) {
                AOC_Print_error(token_node->coordinates, "expected \"}\" but got end of file.");
                break;
            }
        }
        else if (token_node->kind == AOC_TOKEN_KIND_NULL) {
            break;
        }
        uint64_t token_flags = AOC_TokenKind_to_flags[token_node->kind];
        AOC_AstNode *new_ast_node = NULL;
        if (token_node->kind == AOC_TOKEN_KIND_KEYWORD_RETURN) {
            new_ast_node = AOC_ARENA_NEW_ZERO(arena, AOC_AstNode);
            new_ast_node->kind = AOC_AST_NODE_KIND_RETURN;
            new_ast_node->expression = AOC_Parse_expression(arena, token_node->next, &token_node);
        }
        else {
            AOC_Type type = AOC_Parse_type(token_node, &token_node);
            if (type.kind != AOC_AST_TYPE_KIND_NULL) {
                if (token_node->kind == AOC_TOKEN_KIND_IDENTIFIER) {
                    AOC_Token *token_identifier = token_node;
                    token_node=token_node->next;
                    if (token_node->kind == AOC_TOKEN_KIND_OPEN_PAREN) {
                        AOC_Function *function = AOC_Parse_function(arena, token_node, &token_node);
                        function->name        = token_identifier->string;
                        function->return_type = type;
                        if (!scope->functions)      scope->functions = function;
                        if (scope->functions_last)  scope->functions_last->next = function;
                        scope->functions_last = function;
                        scope->functions_count += 1;
                    }
                    else {
                        new_ast_node = AOC_ARENA_NEW_ZERO(arena, AOC_AstNode);
                        new_ast_node->kind = AOC_AST_NODE_KIND_DECLARATION;
                        new_ast_node->declaration = AOC_Parse_value_decl(arena, token_node, &token_node);
                        new_ast_node->declaration->name = token_identifier->string;
                        new_ast_node->declaration->type = type;
                    }
                }
            }
            else {
                token_node = token_node->next;
            }
        }

        if (new_ast_node) {
            if (!result) result = new_ast_node;
            if (last_node) last_node->next = new_ast_node;
            last_node = new_ast_node;
        }
    }
    if (last_token) *last_token = token_node;

    return result;
}

void
AOC_Print_type(AOC_Type type, int32_t ident) {
    printf("%*cTYPE: %.*s", (int)ident, ' ', AOC_STRVARG(AOC_AstTypeKind_to_string[type.kind]));
}

void
AOC_Print_declarations(AOC_Declaration *declarations, int64_t ident) {
    for (AOC_Declaration *declaration = declarations; declaration; declaration=declaration->next) {
        printf("%*c %.*s\n", (int)ident, ' ', AOC_STRVARG(declaration->name));
        AOC_Print_type(declaration->type, ident+2);
        printf("\n");
        if (declaration->assignment) {
            printf("%*cASSIGNMENT: ", (int)ident+2, ' ');
            AOC_Print_expression(declaration->assignment);
            printf("\n");
        }
    }
}

void
AOC_Print_ast(AOC_AstNode *ast_node, int64_t ident) {
    if (!ast_node) {
        return;
    }
    for (;ast_node; ast_node=ast_node->next) {
        if (ast_node->kind == AOC_AST_NODE_KIND_DECLARATION) {
            printf("%*cDECLARATION: \n", (int)ident, ' ');
            AOC_Print_declarations(ast_node->declaration, ident+2);
        }
        else if (ast_node->kind == AOC_AST_NODE_KIND_RETURN) {
            printf("%*cRETURN: ", (int)ident, ' ');
            AOC_Print_expression(ast_node->expression);
            printf("\n");
        }
    }
}

void
AOC_Print_functions(AOC_Function *functions, int64_t ident) {
    for (AOC_Function *function=functions; function; function = function->next) {
        printf("%*cFUNCTION: %.*s\n", (int)ident, ' ', AOC_STRVARG(function->name));
        printf("%*cRETURN\n", (int)ident+2, ' ');
        AOC_Print_type(function->return_type, ident+4);
        printf("\n%*cPARAMETERS\n", (int)ident+2, ' ');
        AOC_Print_declarations(function->parameters, ident+4);
        printf("%*cBODY: \n", (int)ident+2, ' ');
        AOC_Print_ast(function->body, ident+4);
    }
}


enum {
    AOC_WASM_TYPE_CONSTRUCTOR_I32     = 0x7F,
    AOC_WASM_TYPE_CONSTRUCTOR_I64     = 0x7E,
    AOC_WASM_TYPE_CONSTRUCTOR_F32     = 0x7D,
    AOC_WASM_TYPE_CONSTRUCTOR_F64     = 0x7C,
    AOC_WASM_TYPE_CONSTRUCTOR_ANYFUNC = 0x70,
    AOC_WASM_TYPE_CONSTRUCTOR_FUNC    = 0x60,
    AOC_WASM_TYPE_CONSTRUCTOR_EMPTY_BLOCK = 0x40,
};


enum {
    AOC_WASM_EXTERNAL_KIND_FUNCTION = 0,
    AOC_WASM_EXTERNAL_KIND_TABLE    = 1,
    AOC_WASM_EXTERNAL_KIND_MEMORY   = 2,
    AOC_WASM_EXTERNAL_KIND_GLOBAL   = 3,
};


enum {
    AOC_WASM_SECTION_TYPE     = 1,
    AOC_WASM_SECTION_IMPORT   = 2,
    AOC_WASM_SECTION_FUNCTION = 3,
    AOC_WASM_SECTION_TABLE    = 4,
    AOC_WASM_SECTION_MEMORY   = 5,
    AOC_WASM_SECTION_GLOBAL   = 6,
    AOC_WASM_SECTION_EXPORT   = 7,
    AOC_WASM_SECTION_START    = 8,
    AOC_WASM_SECTION_ELEMENT  = 9,
    AOC_WASM_SECTION_CODE     = 10,
    AOC_WASM_SECTION_DATA     = 11,
};


enum {
    AOC_WASM_OPCODE_UNREACHABLE = 0x00, // trap immediately
    AOC_WASM_OPCODE_NOP         = 0x01, // no operation
    AOC_WASM_OPCODE_BLOCK       = 0x02, // sig : block_type begin a sequence of expressions, yielding 0 or 1 values
    AOC_WASM_OPCODE_LOOP        = 0x03, // sig : block_type begin a block which can also form control flow loops
    AOC_WASM_OPCODE_IF          = 0x04, // sig : block_type begin if expression
    AOC_WASM_OPCODE_ELSE        = 0x05, // begin else expression of if
    AOC_WASM_OPCODE_END         = 0x0b, // end a block, loop, or if
    AOC_WASM_OPCODE_BR          = 0x0c, // relative_depth : varuint32 break that targets an outer nested block
    AOC_WASM_OPCODE_BR_IF       = 0x0d, // relative_depth : varuint32 conditional break that targets an outer nested block
    AOC_WASM_OPCODE_BR_TABLE    = 0x0e, // see below branch table control flow construct
    AOC_WASM_OPCODE_RETURN      = 0x0f, // return zero or one value from this function

    AOC_WASM_OPCODE_CALL        = 0x10, // call a function by its index
    AOC_WASM_OPCODE_CALL_INDIRECT = 0x11, // call a function indirect with an expected signature

    AOC_WASM_OPCODE_DROP        = 0x1a, // ignore value [$T]->[]
    AOC_WASM_OPCODE_SELECT      = 0x1b, // select one of two values based on condition [$T,$T]->[$T]

    AOC_WASM_OPCODE_GET_LOCAL   = 0x20, // read a local variable or parameter []->[$T]
    AOC_WASM_OPCODE_SET_LOCAL   = 0x21, // write a local variable or parameter [$T]->[]
    AOC_WASM_OPCODE_TEE_LOCAL   = 0x22, // write a local variable or parameter and return the same value [$T]->[$T]
    AOC_WASM_OPCODE_GET_GLOBAL  = 0x23, // read a global variable []->[$T]
    AOC_WASM_OPCODE_SET_GLOBAL  = 0x24, // write a global variable [$T]->[]

    AOC_WASM_OPCODE_I32_LOAD     = 0x28, // load a i32 from memory  [i32]->[i32]
    AOC_WASM_OPCODE_I64_LOAD     = 0x29, // load a i64 from memory  [i32]->[i64]
    AOC_WASM_OPCODE_F32_LOAD     = 0x2a, // load a f32 from memory  [i32]->[f32]
    AOC_WASM_OPCODE_F64_LOAD     = 0x2b, // load a f64 from memory  [i32]->[f64]
    AOC_WASM_OPCODE_I32_LOAD8_S  = 0x2c, // load a i8 from memory as i32 [i32]->[i32]
    AOC_WASM_OPCODE_I32_LOAD8_U  = 0x2d, // load a u8 from memory as i32 [i32]->[i32]
    AOC_WASM_OPCODE_I32_LOAD16_S = 0x2e, // load a i16 from memory as i32 [i32]->[i32]
    AOC_WASM_OPCODE_I32_LOAD16_U = 0x2f, // load a u16 from memory as i32 [i32]->[i32]
    AOC_WASM_OPCODE_I64_LOAD8_S  = 0x30, // load a i8 from memory as i64  [i32]->[i64]
    AOC_WASM_OPCODE_I64_LOAD8_U  = 0x31, // load a u8 from memory as i64  [i32]->[i64]
    AOC_WASM_OPCODE_I64_LOAD16_S = 0x32, // load a i16 from memory as i64 [i32]->[i64]
    AOC_WASM_OPCODE_I64_LOAD16_U = 0x33, // load a u16 from memory as i64 [i32]->[i64]
    AOC_WASM_OPCODE_I64_LOAD32_S = 0x34, // load a i32 from memory as i64 [i32]->[i64]
    AOC_WASM_OPCODE_I64_LOAD32_U = 0x35, // load a u32 from memory as i64 [i32]->[i64]
    AOC_WASM_OPCODE_I32_STORE    = 0x36, // store a i32 to memory [i32,i32]->[]
    AOC_WASM_OPCODE_I64_STORE    = 0x37, // store a i64 to memory [i32,i64]->[]
    AOC_WASM_OPCODE_F32_STORE    = 0x38, // store a f32 to memory [i32,f32]->[]
    AOC_WASM_OPCODE_F64_STORE    = 0x39, // store a f64 to memory [i32,f64]->[]
    AOC_WASM_OPCODE_I32_STORE8   = 0x3a, // store a i8 to memory using a i32 [i32,i32]->[]
    AOC_WASM_OPCODE_I32_STORE16  = 0x3b, // store a i16 to memory using a i32 [i32,i32]->[]
    AOC_WASM_OPCODE_I64_STORE8   = 0x3c, // store a i8 to memory using a i64 [i32,i64]->[]
    AOC_WASM_OPCODE_I64_STORE16  = 0x3d, // store a i16 to memory using a i64 [i32,i64]->[]
    AOC_WASM_OPCODE_I64_STORE32  = 0x3e, // store a i32 to memory using a i64 [i32,i64]->[]
    AOC_WASM_OPCODE_CURRENT_MEMORY = 0x3f, // query the size of memory
    AOC_WASM_OPCODE_GROW_MEMORY    = 0x40, // grow the size of memory

    AOC_WASM_OPCODE_I32_CONST = 0x41, // a constant value interpreted as i32 []->[i32]
    AOC_WASM_OPCODE_I64_CONST = 0x42, // a constant value interpreted as i64 []->[i64]
    AOC_WASM_OPCODE_F32_CONST = 0x43, // a constant value interpreted as f32 []->[f32]
    AOC_WASM_OPCODE_F64_CONST = 0x44, // a constant value interpreted as f64 []->[f64]

    AOC_WASM_OPCODE_I32_EQZ  = 0x45,
    AOC_WASM_OPCODE_I32_EQ   = 0x46,
    AOC_WASM_OPCODE_I32_NE   = 0x47,
    AOC_WASM_OPCODE_I32_LT_S = 0x48,
    AOC_WASM_OPCODE_I32_LT_U = 0x49,
    AOC_WASM_OPCODE_I32_GT_S = 0x4a,
    AOC_WASM_OPCODE_I32_GT_U = 0x4b,
    AOC_WASM_OPCODE_I32_LE_S = 0x4c,
    AOC_WASM_OPCODE_I32_LE_U = 0x4d,
    AOC_WASM_OPCODE_I32_GE_S = 0x4e,
    AOC_WASM_OPCODE_I32_GE_U = 0x4f,
    AOC_WASM_OPCODE_I64_EQZ  = 0x50,
    AOC_WASM_OPCODE_I64_EQ   = 0x51,
    AOC_WASM_OPCODE_I64_NE   = 0x52,
    AOC_WASM_OPCODE_I64_LT_S = 0x53,
    AOC_WASM_OPCODE_I64_LT_U = 0x54,
    AOC_WASM_OPCODE_I64_GT_S = 0x55,
    AOC_WASM_OPCODE_I64_GT_U = 0x56,
    AOC_WASM_OPCODE_I64_LE_S = 0x57,
    AOC_WASM_OPCODE_I64_LE_U = 0x58,
    AOC_WASM_OPCODE_I64_GE_S = 0x59,
    AOC_WASM_OPCODE_I64_GE_U = 0x5a,
    AOC_WASM_OPCODE_F32_EQ   = 0x5b,
    AOC_WASM_OPCODE_F32_NE   = 0x5c,
    AOC_WASM_OPCODE_F32_LT   = 0x5d,
    AOC_WASM_OPCODE_F32_GT   = 0x5e,
    AOC_WASM_OPCODE_F32_LE   = 0x5f,
    AOC_WASM_OPCODE_F32_GE   = 0x60,
    AOC_WASM_OPCODE_F64_EQ   = 0x61,
    AOC_WASM_OPCODE_F64_NE   = 0x62,
    AOC_WASM_OPCODE_F64_LT   = 0x63,
    AOC_WASM_OPCODE_F64_GT   = 0x64,
    AOC_WASM_OPCODE_F64_LE   = 0x65,
    AOC_WASM_OPCODE_F64_GE   = 0x66,

    AOC_WASM_OPCODE_I32_CLZ    = 0x67,
    AOC_WASM_OPCODE_I32_CTZ    = 0x68,
    AOC_WASM_OPCODE_I32_POPCNT = 0x69,
    AOC_WASM_OPCODE_I32_ADD    = 0x6a,
    AOC_WASM_OPCODE_I32_SUB    = 0x6b,
    AOC_WASM_OPCODE_I32_MUL    = 0x6c,
    AOC_WASM_OPCODE_I32_DIV_S  = 0x6d,
    AOC_WASM_OPCODE_I32_DIV_U  = 0x6e,
    AOC_WASM_OPCODE_I32_REM_S  = 0x6f,
    AOC_WASM_OPCODE_I32_REM_U  = 0x70,
    AOC_WASM_OPCODE_I32_AND    = 0x71,
    AOC_WASM_OPCODE_I32_OR     = 0X72,
    AOC_WASM_OPCODE_I32_XOR    = 0x73,
    AOC_WASM_OPCODE_I32_SHL    = 0x74,
    AOC_WASM_OPCODE_I32_SHR_S  = 0x75,
    AOC_WASM_OPCODE_I32_SHR_U  = 0x76,
    AOC_WASM_OPCODE_I32_ROTL   = 0x77,
    AOC_WASM_OPCODE_I32_ROTR   = 0x78,
    AOC_WASM_OPCODE_I64_CLZ    = 0x79,
    AOC_WASM_OPCODE_I64_CTZ    = 0x7a,
    AOC_WASM_OPCODE_I64_POPCNT = 0x7b,
    AOC_WASM_OPCODE_I64_ADD    = 0x7c,
    AOC_WASM_OPCODE_I64_SUB    = 0x7d,
    AOC_WASM_OPCODE_I64_MUL    = 0x7e,
    AOC_WASM_OPCODE_I64_DIV_S  = 0x7f,
    AOC_WASM_OPCODE_I64_DIV_U  = 0x80,
    AOC_WASM_OPCODE_I64_REM_S  = 0x81,
    AOC_WASM_OPCODE_I64_REM_U  = 0x82,
    AOC_WASM_OPCODE_I64_AND    = 0x83,
    AOC_WASM_OPCODE_I64_OR     = 0X84,
    AOC_WASM_OPCODE_I64_XOR    = 0x85,
    AOC_WASM_OPCODE_I64_SHL    = 0x86,
    AOC_WASM_OPCODE_I64_SHR_S  = 0x87,
    AOC_WASM_OPCODE_I64_SHR_U  = 0x88,
    AOC_WASM_OPCODE_I64_ROTL   = 0x89,
    AOC_WASM_OPCODE_I64_ROTR   = 0x8a,
    AOC_WASM_OPCODE_F32_ABS    = 0x8b,
    AOC_WASM_OPCODE_F32_NEG    = 0x8c,
    AOC_WASM_OPCODE_F32_CEIL   = 0x8d,
    AOC_WASM_OPCODE_F32_FLOOR  = 0x8e,
    AOC_WASM_OPCODE_F32_TRUNC  = 0x8f,
    AOC_WASM_OPCODE_F32_NEAREST = 0x90,
    AOC_WASM_OPCODE_F32_SQRT   = 0x91,
    AOC_WASM_OPCODE_F32_ADD    = 0x92,
    AOC_WASM_OPCODE_F32_SUB    = 0x93,
    AOC_WASM_OPCODE_F32_MUL    = 0x94,
    AOC_WASM_OPCODE_F32_DIV    = 0x95,
    AOC_WASM_OPCODE_F32_MIN    = 0x96,
    AOC_WASM_OPCODE_F32_MAX    = 0x97,
    AOC_WASM_OPCODE_F32_COPYSIGN = 0x98,
    AOC_WASM_OPCODE_F64_ABS    = 0x99,
    AOC_WASM_OPCODE_F64_NEG    = 0x9a,
    AOC_WASM_OPCODE_F64_CEIL   = 0x9b,
    AOC_WASM_OPCODE_F64_FLOOR  = 0x9c,
    AOC_WASM_OPCODE_F64_TRUNC  = 0x9d,
    AOC_WASM_OPCODE_F64_NEAREST = 0x9e,
    AOC_WASM_OPCODE_F64_SQRT   = 0x9f,
    AOC_WASM_OPCODE_F64_ADD    = 0xa0,
    AOC_WASM_OPCODE_F64_SUB    = 0xa1,
    AOC_WASM_OPCODE_F64_MUL    = 0xa2,
    AOC_WASM_OPCODE_F64_DIV    = 0xa3,
    AOC_WASM_OPCODE_F64_MIN    = 0xa4,
    AOC_WASM_OPCODE_F64_MAX    = 0xa5,
    AOC_WASM_OPCODE_F64_COPYSIGN = 0xa6,

    AOC_WASM_OPCODE_I32_WRAP_I64      = 0XA7,
    AOC_WASM_OPCODE_I32_TRUNC_S_F32   = 0xa8,
    AOC_WASM_OPCODE_I32_TRUNC_U_F32   = 0xa9,
    AOC_WASM_OPCODE_I32_TRUNC_S_F64   = 0xaa,
    AOC_WASM_OPCODE_I32_TRUNC_U_F64   = 0xab,
    AOC_WASM_OPCODE_I64_EXTEND_S_I32  = 0xac,
    AOC_WASM_OPCODE_I64_EXTEND_U_I32  = 0xad,
    AOC_WASM_OPCODE_I64_TRUNC_S_F32   = 0xae,
    AOC_WASM_OPCODE_I64_TRUNC_U_F32   = 0xaf,
    AOC_WASM_OPCODE_I64_TRUNC_S_F64   = 0xb0,
    AOC_WASM_OPCODE_I64_TRUNC_U_F64   = 0xb1,
    AOC_WASM_OPCODE_F32_CONVERT_S_I32 = 0xb2,
    AOC_WASM_OPCODE_F32_CONVERT_U_I32 = 0xb3,
    AOC_WASM_OPCODE_F32_CONVERT_S_I64 = 0xb4,
    AOC_WASM_OPCODE_F32_CONVERT_U_I64 = 0xb5,
    AOC_WASM_OPCODE_F32_DEMOTE_F64    = 0XB6,
    AOC_WASM_OPCODE_F64_CONVERT_S_I32 = 0xb7,
    AOC_WASM_OPCODE_F64_CONVERT_U_I32 = 0xb8,
    AOC_WASM_OPCODE_F64_CONVERT_S_I64 = 0xb9,
    AOC_WASM_OPCODE_F64_CONVERT_U_I64 = 0xba,
    AOC_WASM_OPCODE_F64_PROMOTE_F32   = 0xbb,
    // More than 1 byte :(
    // AOC_WASM_OPCODE_I32_TRUNC_SAT_F32_S 0xfc 0x00
    // AOC_WASM_OPCODE_I32_TRUNC_SAT_F32_U 0xfc 0x01
    // AOC_WASM_OPCODE_I32_TRUNC_SAT_F64_S 0xfc 0x02
    // AOC_WASM_OPCODE_I32_TRUNC_SAT_F64_U 0xfc 0x03
    // AOC_WASM_OPCODE_I64_TRUNC_SAT_F32_S 0xfc 0x04
    // AOC_WASM_OPCODE_I64_TRUNC_SAT_F32_U 0xfc 0x05
    // AOC_WASM_OPCODE_I64_TRUNC_SAT_F64_S 0xfc 0x06
    // AOC_WASM_OPCODE_I64_TRUNC_SAT_F64_U 0xfc 0x07

    AOC_WASM_OPCODE_I32_REINTERPRET_F32 = 0xbc,
    AOC_WASM_OPCODE_I64_REINTERPRET_F64 = 0xbd,
    AOC_WASM_OPCODE_F32_REINTERPRET_I32 = 0xbe,
    AOC_WASM_OPCODE_F64_REINTERPRET_I64 = 0xbf,
};



int64_t
AOC_Varuint_size(uint64_t value) {
    if (value & (0x7Full<<56)) return 9;
    if (value & (0x7Full<<49)) return 8;
    if (value & (0x7Full<<42)) return 7;
    if (value & (0x7Full<<35)) return 6;
    if (value & (0x7Full<<28)) return 5;
    if (value & (0x7Full<<21)) return 4;
    if (value & (0x7Full<<14)) return 3;
    if (value & (0x7Full<<7))  return 2;
    else return 1;
}


int64_t
AOC_Varint_size(int64_t value) {
    if (value < -(1ll<<55) || value > (1ll<<55)) return 9;
    if (value < -(1ll<<48) || value > (1ll<<48)) return 8;
    if (value < -(1ll<<41) || value > (1ll<<41)) return 7;
    if (value < -(1ll<<34) || value > (1ll<<34)) return 6;
    if (value < -(1ll<<27) || value > (1ll<<27)) return 5;
    if (value < -(1ll<<20) || value > (1ll<<20)) return 4;
    if (value < -(1ll<<13) || value > (1ll<<13)) return 3;
    if (value < -(1ll<<6) || value > (1<<6)) return 2;
    else return 1;
}


typedef struct AOC_WasmFuncType AOC_WasmFuncType;
struct AOC_WasmFuncType {
    uint32_t params_count;
    uint32_t returns_count;
    uint8_t *params;
    uint8_t *returns;
};


typedef struct {
    uint32_t types_count;
    AOC_WasmFuncType *types;
} AOC_WasmModuleTypes;


int64_t
AOC_WasmModuleTypes_Compute_size(AOC_WasmModuleTypes types_section) {
    int64_t result = 0;
    result = result + AOC_Varuint_size(types_section.types_count);
    for (int64_t type_i = 0; type_i < types_section.types_count; type_i+=1) {
        AOC_WasmFuncType type = types_section.types[type_i];
        result += 1; // Function type constructor
        result = result + AOC_Varuint_size(type.params_count);
        result = result + type.params_count;
        result = result + AOC_Varuint_size(type.returns_count);
        result = result + type.returns_count;
    }
    return result;
}


typedef struct {
    uint32_t functions_count;
    uint32_t *type_indices;
} AOC_WasmModuleFunctions;


int64_t
AOC_WasmModuleFunctions_Compute_size(AOC_WasmModuleFunctions functions_section) {
    int64_t result = 0;
    result = result + AOC_Varuint_size(functions_section.functions_count);
    for (int64_t function_i = 0; function_i < functions_section.functions_count; function_i+=1) {
        result += AOC_Varuint_size(functions_section.type_indices[function_i]);
    }
    return result;
}


typedef struct {
    AOC_String symbol_name;
    uint32_t kind;
    uint32_t index;
} AOC_WasmModuleExportEntry;


typedef struct {
    uint32_t entries_count;
    AOC_WasmModuleExportEntry *entries;
} AOC_WasmModuleExports;


int64_t
AOC_WasmModuleExports_Compute_size(AOC_WasmModuleExports exports_section) {
    int64_t result = 0;
    result = result + AOC_Varuint_size(exports_section.entries_count);
    for (int64_t entry_i = 0; entry_i < exports_section.entries_count; entry_i+=1) {
        AOC_WasmModuleExportEntry entry = exports_section.entries[entry_i];
        result += AOC_Varuint_size(entry.symbol_name.len);
        result += entry.symbol_name.len;
        result += AOC_Varuint_size(entry.kind);
        result += AOC_Varuint_size(entry.index);
    }
    return result;
}

typedef struct {
    uint32_t count;
    uint32_t type;
} AOC_WasmModuleLocalEntry;

typedef struct {
    uint32_t locals_count;
    AOC_WasmModuleLocalEntry *locals;
    uint32_t bytecode_size;
    uint8_t *bytecode;
} AOC_WasmModuleBody;


int64_t
AOC_WasmModuleBody_Compute_size(AOC_WasmModuleBody body) {
    int64_t result = 0;
    result = result + AOC_Varuint_size(body.locals_count);
    for (int64_t local_i = 0; local_i < body.locals_count; local_i+=1) {
        AOC_WasmModuleLocalEntry local = body.locals[local_i];
        result += AOC_Varuint_size(local.count);
        result += AOC_Varuint_size(local.type);
    }
    result += body.bytecode_size;
    return result;
}


typedef struct {
    uint32_t bodies_count;
    AOC_WasmModuleBody *bodies;
} AOC_WasmModuleCode;


int64_t
AOC_WasmModuleCode_Compute_size(AOC_WasmModuleCode code) {
    int64_t result = 0;
    result = result + AOC_Varuint_size(code.bodies_count);
    for (int64_t body_i = 0; body_i < code.bodies_count; body_i+=1) {
        int64_t body_size = AOC_WasmModuleBody_Compute_size(code.bodies[body_i]);
        result += AOC_Varuint_size(body_size);
        result += body_size;
    }
    return result;
}

typedef struct {
    AOC_WasmModuleTypes type_section;
    AOC_WasmModuleFunctions function_section;
    AOC_WasmModuleExports export_section;
    AOC_WasmModuleCode code_section;
} AOC_WasmModule;


uint8_t
AOC_AST_Type_to_Wasm_type(AOC_Type type) {
    if (type.kind == AOC_AST_TYPE_KIND_LONG_LONG_INT || type.kind == AOC_AST_TYPE_KIND_UNSIGNED_LONG_LONG_INT) {
        return AOC_WASM_TYPE_CONSTRUCTOR_I64;
    }
    else if (type.kind == AOC_AST_TYPE_KIND_FLOAT) {
        return AOC_WASM_TYPE_CONSTRUCTOR_F32;
    }
    else if (type.kind == AOC_AST_TYPE_KIND_DOUBLE) {
        return AOC_WASM_TYPE_CONSTRUCTOR_F64;
    }
    else {
        return AOC_WASM_TYPE_CONSTRUCTOR_I32;
    }
}

uint8_t *
AOC_Expression_to_bytecode(uint8_t *bytecode, AOC_Expression *expression) {
    if (expression->kind == AOC_EXPRESSION_KIND_NUMERIC_VALUE) {
        bytecode[0] = AOC_WASM_OPCODE_I32_CONST;
        bytecode[1] = 0;
        bytecode = &bytecode[2];
    }
    else if (expression->kind == AOC_EXPRESSION_KIND_IDENTIFIER_VALUE) {
        bytecode[0] = AOC_WASM_OPCODE_I32_CONST;
        bytecode[1] = 0;
        bytecode = &bytecode[2];
    }
    else if (AOC_Expression_is_binary(expression)) {
        bytecode = AOC_Expression_to_bytecode(bytecode, expression->binary.left);
        bytecode = AOC_Expression_to_bytecode(bytecode, expression->binary.right);
        if (expression->kind == AOC_EXPRESSION_KIND_ADD) {
            bytecode[0] = AOC_WASM_OPCODE_I32_ADD;
            bytecode = &bytecode[1];
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_SUBSTRACT) {
            bytecode[0] = AOC_WASM_OPCODE_I32_SUB;
            bytecode = &bytecode[1];
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_MULTIPLY) {
            bytecode[0] = AOC_WASM_OPCODE_I32_MUL;
            bytecode = &bytecode[1];
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_DIVIDE) {
            bytecode[0] = AOC_WASM_OPCODE_I32_DIV_S;
            bytecode = &bytecode[1];
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_AND) {
            bytecode[0] = AOC_WASM_OPCODE_I32_AND;
            bytecode = &bytecode[1];
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_OR) {
            bytecode[0] = AOC_WASM_OPCODE_I32_OR;
            bytecode = &bytecode[1];
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_EQUAL) {
            bytecode[0] = AOC_WASM_OPCODE_I32_EQ;
            bytecode = &bytecode[1];
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_LESS) {
            bytecode[0] = AOC_WASM_OPCODE_I32_LT_S;
            bytecode = &bytecode[1];
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_LESS_EQUAL) {
            bytecode[0] = AOC_WASM_OPCODE_I32_LE_S;
            bytecode = &bytecode[1];
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_GREATER) {
            bytecode[0] = AOC_WASM_OPCODE_I32_GT_S;
            bytecode = &bytecode[1];
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_GREATER_EQUAL) {
            bytecode[0] = AOC_WASM_OPCODE_I32_GE_S;
            bytecode = &bytecode[1];
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_ACCESS_FIELD) {
            printf("Access field not implemented yet\n");
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_DEREF_ACCESS_FIELD) {
            printf("Access field not implemented yet\n");
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_ASSIGN) {
            printf("Assign not implemented yet\n");
        }
    }
    else if (AOC_Expression_is_unary(expression)) {
        bytecode = AOC_Expression_to_bytecode(bytecode, expression->unary.child);
        if (expression->kind == AOC_EXPRESSION_KIND_PLUS) {
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_MINUS) {
            bytecode[0] = AOC_WASM_OPCODE_I32_CONST;
            bytecode[1] = 1;
            bytecode[2] = AOC_WASM_OPCODE_I32_SUB;
            bytecode[3] = AOC_WASM_OPCODE_I32_CONST;
            bytecode[4] = 0x7F;
            bytecode[5] = AOC_WASM_OPCODE_I32_XOR;
            bytecode = &bytecode[6];
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_INVERT) {
            bytecode[0] = AOC_WASM_OPCODE_I32_CONST;
            bytecode[1] = 0x7F;
            bytecode[2] = AOC_WASM_OPCODE_I32_XOR;
            bytecode = &bytecode[3];
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_PREINCREMENT) {
            printf("Preincrement not implemented\n");
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_PREDECREMENT) {
            printf("Predecrement not implemented\n");
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_DEREFERENCE) {
            printf("Dereference not implemented\n");
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_REFERENCE) {
            printf("Referencde not implemented\n");
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_CAST) {
            printf("Cast not implemented\n");
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_POSTINCREMENT) {
            printf("Postincrement not implemented\n");
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_POSTDECREMENT) {
            printf("Postdecrement not implemented\n");
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_FUNCTION_CALL) {
            printf("Function call not implemented\n");
        }
        else if (expression->kind == AOC_EXPRESSION_KIND_INDEXING) {
            printf("Indexing not implemented\n");
        }
        else {
            assert(false);
        }
    }
    else {
        assert(false);
    }
    return bytecode;
}

bool
AOC_Emit_wasm(AOC_Arena *arena, AOC_Scope *scope, AOC_WasmModule *result) {
    int64_t functions_count = scope->functions_count;

    AOC_WasmModuleTypes type_section = {0};
    type_section.types_count = functions_count;
    type_section.types = AOC_ARENA_NEW_ARRAY(arena, functions_count, AOC_WasmFuncType);

    AOC_WasmModuleFunctions function_section = {0};
    function_section.functions_count = functions_count;
    function_section.type_indices = AOC_ARENA_NEW_ARRAY(arena, functions_count, uint32_t);

    AOC_WasmModuleExports export_section = {0};
    export_section.entries_count = functions_count;
    export_section.entries = AOC_ARENA_NEW_ARRAY(arena, functions_count, AOC_WasmModuleExportEntry);

    AOC_WasmModuleCode code_section = {0};
    code_section.bodies_count = functions_count;
    code_section.bodies = AOC_ARENA_NEW_ARRAY(arena, functions_count, AOC_WasmModuleBody);

    int64_t function_i = 0;
    for (AOC_Function *function = scope->functions; function; function=function->next, function_i+=1) {
        type_section.types[function_i].returns_count = 0;

        AOC_Type return_type = function->return_type;

        if (return_type.kind != AOC_AST_TYPE_KIND_VOID) {
            type_section.types[function_i].returns_count = 1;
            type_section.types[function_i].returns = AOC_ARENA_NEW_ARRAY(arena, 1, uint8_t);
            type_section.types[function_i].returns[0] = AOC_AST_Type_to_Wasm_type(return_type);
        }


        type_section.types[function_i].params_count = function->parameters_count;
        if (function->parameters_count > 0) {
            type_section.types[function_i].params = AOC_ARENA_NEW_ARRAY(arena, function->parameters_count, uint8_t);
        }

        int64_t param_i = 0;
        for (AOC_Declaration *parameter=function->parameters; parameter; parameter=parameter->next,param_i+=1) {
            type_section.types[function_i].params[param_i] = AOC_AST_Type_to_Wasm_type(parameter->type);
        }

        function_section.type_indices[function_i] = function_i;
        export_section.entries[function_i].symbol_name = function->name;
        export_section.entries[function_i].kind  = AOC_WASM_EXTERNAL_KIND_FUNCTION;
        export_section.entries[function_i].index = function_i;
        
        uint8_t *bytecode_begin = AOC_ARENA_NEW_ARRAY(arena, 1024, uint8_t);
        uint8_t *bytecode_curr  = bytecode_begin;
        for (AOC_AstNode *ast_node = function->body; ast_node; ast_node=ast_node->next) {
            if (ast_node->kind == AOC_AST_NODE_KIND_RETURN) {
                if (ast_node->expression) {
                    bytecode_curr = AOC_Expression_to_bytecode(bytecode_curr, ast_node->expression);
                }
                bytecode_curr[0] = AOC_WASM_OPCODE_RETURN;
                bytecode_curr    = &bytecode_curr[1];
            }
        }
        bytecode_curr[0] = AOC_WASM_OPCODE_END;
        bytecode_curr    = &bytecode_curr[1];

        code_section.bodies[function_i].bytecode = bytecode_begin;
        code_section.bodies[function_i].bytecode_size = bytecode_curr-bytecode_begin;
        code_section.bodies[function_i].locals_count = 0;
    }

    result->type_section = type_section;
    result->function_section = function_section;
    result->export_section = export_section;
    result->code_section = code_section;

    return true;
}



#ifndef TESTS


#include <stdio.h>


bool
AOC_Read_entire_file(AOC_Arena *arena, const char path[], AOC_String *result) {
    bool errors = false;
    
    FILE *file = NULL;
    if (!errors) {
        file = fopen(path, "rb");
        if (!file) errors = true;
    }
    
    int64_t file_size = 0;
    uint8_t *data     = NULL;
    if (!errors) {
        fseek(file, 0, SEEK_END);
        file_size = ftell(file);
        fseek(file, 0, SEEK_SET);
        data = AOC_ARENA_NEW_ARRAY(arena, file_size, uint8_t);
        if (!data) errors = true;
    }

    if (!errors) {
        if (file_size != fread(data, sizeof(uint8_t), file_size, file)) {
            errors = true;
        }
    }

    if (file) fclose(file);

    // TODO free the arena if we have errors!
    if (result) {
        result->data = data;
        result->len  = file_size;
    }

    bool success = !errors;
    return success;
}



bool
AOC_Write_varuint(FILE *file, uint64_t value) {
    
    bool end = false;
    for (;!end;) {

        uint8_t byte = value&0x7F;
        value = value >> 7;

        if (value == 0) end = true;
        else            byte |= 0x80;

        if (putc(byte, file) == EOF) return false;
    }

    return true;
}


bool
AOC_Write_varint(FILE *file, int64_t value) {

    bool end = false;
    uint64_t mask = 0xFFFFFFFFFFFFFFFF;
    uint64_t valueu = (uint64_t)valueu;
    for (;!end;) {
        
        uint8_t byte = valueu&0x7F;
        valueu = valueu>>7;
        mask   = mask>>7;

        if (valueu == 0 || ((valueu^mask)==0 && (byte&0x40))) end = true;
        else byte |= 0x80;

        if (putc(byte, file) == EOF) return false;
    }

    return true;
}


bool
AOC_Write_section_header(FILE *file, int8_t id, uint32_t payload_len) {

    if (putc(id, file) == EOF) return false;
    if (!AOC_Write_varuint(file, payload_len)) return false;

    return true;
}




bool
AOC_Save_wasm_to_file(FILE *file, AOC_WasmModule *module) {

    const char magic[] = {'\0','a','s','m'};
    if (fwrite(magic, sizeof(magic), 1, file) != 1) return false;

    int32_t version = 1;
    if (fwrite(&version, sizeof(version), 1, file) != 1) return false;



    // ----------------------------
    // Type section
    // ---------------------------
    {
        AOC_WasmModuleTypes type_section = module->type_section;

        if (!AOC_Write_section_header(file, AOC_WASM_SECTION_TYPE, AOC_WasmModuleTypes_Compute_size(type_section))) return false;

        if (!AOC_Write_varuint(file, type_section.types_count)) return false;

        for (int type_i = 0; type_i < type_section.types_count; type_i+=1) {
            
            AOC_WasmFuncType type = type_section.types[type_i];

            if (putc(AOC_WASM_TYPE_CONSTRUCTOR_FUNC, file) == EOF) return false;

            if (!AOC_Write_varuint(file, type.params_count)) return false;

            for (int param_i = 0; param_i < type.params_count; param_i+=1) {
                    if (!AOC_Write_varuint(file, type.params[param_i])) return false;
            }

            if (!AOC_Write_varuint(file, type.returns_count)) return false;

            for (int return_i = 0; return_i < type.returns_count; return_i+=1) {
                if (!AOC_Write_varuint(file, type.returns[return_i])) return false;
            }
        }
    }


    // -------------------------------
    // Function section
    // -------------------------------
    {
        AOC_WasmModuleFunctions function_section = module->function_section;
        uint64_t section_size = AOC_WasmModuleFunctions_Compute_size(function_section);
        if (!AOC_Write_section_header(file, AOC_WASM_SECTION_FUNCTION, section_size)) return false;

        if (!AOC_Write_varuint(file, function_section.functions_count)) return false;

        for (int64_t function_i=0; function_i < function_section.functions_count; function_i+=1) {
            if (!AOC_Write_varuint(file, function_section.type_indices[function_i])) return false;
        }
    }


    // ----------------------------
    // EXPORT section
    // ----------------------------
    {
        AOC_WasmModuleExports export_section = module->export_section;
        uint64_t section_size = AOC_WasmModuleExports_Compute_size(export_section);
        if (!AOC_Write_section_header(file, AOC_WASM_SECTION_EXPORT, section_size)) return false;

        if (!AOC_Write_varuint(file, export_section.entries_count)) return false;

        for (int64_t entry_i=0; entry_i < export_section.entries_count; entry_i+=1) {

            AOC_WasmModuleExportEntry entry = export_section.entries[entry_i];
            
            if (!AOC_Write_varuint(file, entry.symbol_name.len)) return false;

            if (fwrite(entry.symbol_name.data, entry.symbol_name.len, 1, file) != 1) return false;

            if (!AOC_Write_varuint(file, entry.kind)) return false;

            if (!AOC_Write_varuint(file, entry.index)) return false;
        }

    }



    // -----------------------------
    // CODE Section
    // -----------------------------
    {
        AOC_WasmModuleCode code_section = module->code_section;
        uint64_t section_size = AOC_WasmModuleCode_Compute_size(code_section);
        if (!AOC_Write_section_header(file, AOC_WASM_SECTION_CODE, section_size)) return false;

        if (!AOC_Write_varuint(file, code_section.bodies_count)) return false;

        for (int64_t body_i=0; body_i < code_section.bodies_count; body_i+=1) {
            AOC_WasmModuleBody body = code_section.bodies[body_i];

            if (!AOC_Write_varuint(file, AOC_WasmModuleBody_Compute_size(body))) return false;

            if (!AOC_Write_varuint(file, body.locals_count)) return false;

            for (int64_t local_i=0; local_i < body.locals_count; local_i+=1) {

                AOC_WasmModuleLocalEntry local = body.locals[local_i];

                if (!AOC_Write_varuint(file, local.count)) return false;

                if (!AOC_Write_varuint(file, local.type)) return false;
            }
        
            if (fwrite(body.bytecode, body.bytecode_size, 1, file) != 1) return false;
        }
    }

    return true;
}


bool
AOC_Save_wasm(const char path[], AOC_WasmModule *module) {
    bool errors = false;

    FILE *file = NULL;
    if (!errors) {
        file = fopen(path, "wb");
        if (!file) errors = true;
    }

    if (!errors) {
        if (!AOC_Save_wasm_to_file(file, module)) errors = true;
    }

    if (file) fclose(file);

    bool success = !errors;
    return success;
}



int
main(void) {
    AOC_String input;

    AOC_Arena *arena = AOC_Arena_Allocate();

    if (!AOC_Read_entire_file(arena, "input.c", &input)) {
        printf("Failed to load the file input.c");
        return -1;
    }

    AOC_Token *token_node = AOC_Tokenize(arena, input);
    for (AOC_Token *t = token_node; t; t=t->next) {
        AOC_Print_token(*t);
        printf("\n");
    }

    AOC_Scope scope = {0};
    AOC_AstNode *ast_node = AOC_Parse(arena, &scope, false, token_node, NULL);
    AOC_Print_functions(scope.functions, 0);

    uint8_t code[] = {
        AOC_WASM_OPCODE_GET_LOCAL, 0,
        AOC_WASM_OPCODE_GET_LOCAL, 1,
        AOC_WASM_OPCODE_I32_ADD,
        AOC_WASM_OPCODE_RETURN,
        AOC_WASM_OPCODE_END,
    };

    AOC_WasmModule module 
    #if 0
    = {

        .type_section = {
            .types_count = 1,
            .types = (AOC_WasmFuncType[]){
                {
                    // TYPE Function (i32, i32) -> i32
                    .params_count = 2,
                    .returns_count = 1,
                    .params  = (uint8_t[]) {AOC_WASM_TYPE_CONSTRUCTOR_I32, AOC_WASM_TYPE_CONSTRUCTOR_I32},
                    .returns = (uint8_t[]) {AOC_WASM_TYPE_CONSTRUCTOR_I32}
                }
            }
        },

        .function_section = {
            .functions_count = 1,
            .type_indices = (uint32_t[]){0},
        },

        .export_section = {
            .entries_count = 1,
            .entries = (AOC_WasmModuleExportEntry[]) {
                {
                    .symbol_name = AOC_STRLIT("add"),
                    .kind        = AOC_WASM_EXTERNAL_KIND_FUNCTION,
                    .index       = 0,
                }
            }
        },

        .code_section = {
            .bodies_count = 1,
            .bodies = (AOC_WasmModuleBody[]) {
                {
                    .locals_count = 0,
                    .bytecode_size = sizeof(code),
                    .bytecode = code
                }
            }
        },

    }
    #endif
    ;

    AOC_Emit_wasm(arena, &scope, &module);

    if (!AOC_Save_wasm("out.wasm", &module)) {
        printf("Failed write the file out.wasm");
    }

    return 0;
}


#else

#include "utest.h"

UTEST(utf8, utf8) {
    {
        AOC_String s = AOC_STRLIT("$");
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(s, 0, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0x24);
        ASSERT_EQ(rune_byte_size, 1);
    }
    {
        AOC_String s = AOC_STRLIT("£");
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(s, 0, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0xA3);
        ASSERT_EQ(rune_byte_size, 2);
    }
    {
        AOC_String s = AOC_STRLIT("ह");
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(s, 0, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0x939);
        ASSERT_EQ(rune_byte_size, 3);
    }
    {
        AOC_String s = AOC_STRLIT("€");
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(s, 0, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0x20AC);
        ASSERT_EQ(rune_byte_size, 3);
    }
    {
        AOC_String s = AOC_STRLIT("한");
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(s, 0, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0xD55C);
        ASSERT_EQ(rune_byte_size, 3);
    }
    {
        AOC_String s = AOC_STRLIT("𐍈");
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_TRUE(AOC_Extract_rune(s, 0, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0x10348);
        ASSERT_EQ(rune_byte_size, 4);
    }
    {
        uint8_t data[] = {0xF0,0xFF,0xFF,0xFF};
        AOC_String s;
        s.data = data;
        s.len  = sizeof(data);
        uint32_t rune;
        int64_t  rune_byte_size;
        ASSERT_FALSE(AOC_Extract_rune(s, 0, &rune, &rune_byte_size));
        ASSERT_EQ(rune, 0);
        ASSERT_EQ(rune_byte_size, 4);
    }
}


UTEST(number_conv, number_conv) {

    AOC_Number n = AOC_String_to_number(AOC_STRLIT("0"));
    ASSERT_EQ(n.kind, AOC_NUMBER_KIND_UNSIGNED_LONG_LONG_INT);
    ASSERT_EQ(n.value_uint, 0);
    

    n = AOC_String_to_number(AOC_STRLIT("23480"));
    ASSERT_EQ(n.kind, AOC_NUMBER_KIND_UNSIGNED_LONG_LONG_INT);
    ASSERT_EQ(n.value_uint, 23480);


    n = AOC_String_to_number(AOC_STRLIT("0xFF"));
    ASSERT_EQ(n.kind, AOC_NUMBER_KIND_UNSIGNED_LONG_LONG_INT);
    ASSERT_EQ(n.value_uint, 0xFF);

    n = AOC_String_to_number(AOC_STRLIT("010"));
    ASSERT_EQ(n.kind, AOC_NUMBER_KIND_UNSIGNED_LONG_LONG_INT);
    ASSERT_EQ(n.value_uint, 8);

    n = AOC_String_to_number(AOC_STRLIT("0104"));
    ASSERT_EQ(n.kind, AOC_NUMBER_KIND_UNSIGNED_LONG_LONG_INT);
    ASSERT_EQ(n.value_uint, 0104);

    n = AOC_String_to_number(AOC_STRLIT("0B100"));
    ASSERT_EQ(n.kind, AOC_NUMBER_KIND_UNSIGNED_LONG_LONG_INT);
    ASSERT_EQ(n.value_uint, 0b100);

    n = AOC_String_to_number(AOC_STRLIT("0B1100"));
    ASSERT_EQ(n.kind, AOC_NUMBER_KIND_UNSIGNED_LONG_LONG_INT);
    ASSERT_EQ(n.value_uint, 0b1100);

    n = AOC_String_to_number(AOC_STRLIT(".5"));
    ASSERT_EQ(n.kind, AOC_NUMBER_KIND_DOUBLE);
    ASSERT_EQ(n.value_double, .5);

    n = AOC_String_to_number(AOC_STRLIT("1.4e10"));
    ASSERT_EQ(n.kind, AOC_NUMBER_KIND_DOUBLE);
    ASSERT_EQ(n.value_double, 1.4e10);

    n = AOC_String_to_number(AOC_STRLIT("23847.534721"));
    ASSERT_EQ(n.kind, AOC_NUMBER_KIND_DOUBLE);
    ASSERT_EQ(n.value_double, 23847.534721);

    n = AOC_String_to_number(AOC_STRLIT("laskdfj"));
    ASSERT_EQ(n.kind, AOC_NUMBER_KIND_NULL);
    n = AOC_String_to_number(AOC_STRLIT("0xZ"));
    ASSERT_EQ(n.kind, AOC_NUMBER_KIND_NULL);
    n = AOC_String_to_number(AOC_STRLIT("0xZ"));
    ASSERT_EQ(n.kind, AOC_NUMBER_KIND_NULL);
    n = AOC_String_to_number(AOC_STRLIT("0x0.5"));
    ASSERT_EQ(n.kind, AOC_NUMBER_KIND_NULL);
}


UTEST(arena, arena) {
    AOC_Arena *arena1 = AOC_Arena_Allocate();

    AOC_Arena_Push_aligned(arena1, 1024, 1);
    ASSERT_EQ(arena1->current->cursor, 1024);
    AOC_Arena_Push_aligned(arena1, AOC_ARENA_CAPACITY, 1);
    ASSERT_EQ(arena1->current->cursor, AOC_ARENA_CAPACITY);
    ASSERT_TRUE(arena1->current->prev != NULL);
    
    AOC_Arena_Release(arena1);
    ASSERT_TRUE(AOC_free_arenas       != NULL);
    ASSERT_TRUE(AOC_free_arenas->prev != NULL);
    ASSERT_TRUE(AOC_free_arenas->prev->prev == NULL);

    AOC_Arena *arena2 = AOC_Arena_Allocate();
    AOC_Arena *arena3 = AOC_Arena_Allocate();
    ASSERT_TRUE(AOC_free_arenas == NULL);
}

UTEST_MAIN();

#endif


